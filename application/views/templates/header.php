
<!DOCTYPE html>
<html lang="id-ID">

<head>
    <title>Varia Peradilan - Majalah Hukum</title>
    <meta name="author" content="Ikatan Hakim Indonesia">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="keywords" content="Varia Peradilan, Majalah Hukum, Ikahi, Ikatan Hakim Indonesia">
    <meta name="description" content="Varia Peradilan - Majalah Hukum Digital">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url() ?>dist/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url() ?>dist/images/favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800|Poppins:300i,400,300,700,400i,500|Pacifico:400%2C700|Raleway:400,500,600,700" rel="stylesheet">
    <!-- CSS Files -->

    <link href="<?php echo base_url() ?>dist/css<?php echo elixir("style.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plyr/plyr.css" rel="stylesheet">
    
    <script src="https://unpkg.com/pdfjs-dist@2.0.489/build/pdf.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/js<?php echo elixir("bundle.js") ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plyr/plyr.min.js"></script>
</head>

<body>

   <!-- Header LAYOUT 6  -->
    <header class="header-style-2">
        <div class="background-dark padding-tb-5px">
            <div class="container">
                <div class="row">
                    
                    <div class="col-6">
                        <!-- <ul class="list-inline text-left margin-0px text-white">
                            <li class="list-inline-item"><a class="facebook" href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="youtube" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="google" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                        </ul> -->
                        <!-- // Social -->
                    </div>
                    <div class="col-6 text-right">
                        <div class="contact-info text-white">
                            <!-- <span class="margin-right-10px">Call us : 0022290411185</span>
                            <span>Email: info@localhost.com</span> -->
                            <?php
                            

                            if (!$this->ion_auth->logged_in()) { ?>
                              <a href="<?php echo base_url() ?>auth/login" class="mr-3">Login</a>
                              <a href="<?php echo base_url() ?>auth/register">Daftar</a>
                            <?php 
                            } else{ 
                                $user = $this->ion_auth->user()->row();
                            ?>
                                <span>Hallo, <?php echo $user->fullname ?> | <a href="<?php echo base_url() ?>auth/logout">Logout</a></span>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- <div class="col-lg-3  d-none d-lg-block">
                        <ul class="user-area list-inline float-right margin-0px text-white">
                            <li class="list-inline-item  padding-right-10px"><a href="page-login-2.html"><i class="fa fa-lock padding-right-5px"></i>login</a></li>
                            <li class="list-inline-item"><a href="page-login-1.html"><i class="fa fa-user-plus padding-right-5px"></i>register</a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="row no-gutters">
                <div class="col-3 padding-top-5px" style="background: #B01946"></div>
                <div class="col-3" style="background: #9BB424"></div>
                <div class="col-3" style="background: #4188F0"></div>
                <div class="col-3" style="background: #DCA100"></div>
            </div>
        <div class="header-output">

            <div class="container">
                <div class="row">
                    <div class="col-lg-12 logo-in text-center">
                        <a id="logo" href="<?php echo base_url() ?>" class="d-inline-block margin-tb-20px"><img src="<?php echo base_url() ?>dist/images/logo.png" alt=""></a>
                        <a class="mobile-toggle" href="#"><i class="fa fa-bars"></i></a>
                        <div class="clearfix"></div>
                    </div>
                                <div class="col-12">
                                    
                                </div>
                    <div class="col-lg-12 inherit-menu header-in border-top-1 border-grey">

                       

                        <div class="search-link pull-right model-link">
                            <a id="search-header" class="model-link" href="#"><i class="fa fa-search"></i></a>
                        </div>

                        <!-- Menu Main -->
                        <ul id="menu-main" class="nav-menu float-left link-padding-tb-15px">
                            <li><a href="<?php echo base_url() ?>">Beranda</a></li>
                            <li><a href="<?php echo base_url() ?>collection">Edisi Lainnya</a></li>
                            <!-- <li><a href="<?php echo base_url() ?>subscribe/pricing">Berlangganan</a></li> -->
                        </ul>

                        <!-- end Menu Main -->

                    </div>
                </div>
                <div id="search_bar">
                    <a href="#" class="close-search"><i class="fa fa-close"></i></a>
                    <input type="text" class="input-search full-width" placeholder="Masukan kata kunci...">
                </div>
            </div>
        </div>
    </header>
    <!-- // Header  -->


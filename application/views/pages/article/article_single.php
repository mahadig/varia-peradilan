
    <!-- Page title --->
    <section class="background-grey-1 padding-tb-7px text-grey-4 text-center">
        <div class="container text-center">
            <!-- <h6 class="font-weight-300 text-capitalize mb-0 float-md-left font-2 padding-tb-10px">Lorem Ipsum Dolor Sit amet, Consectetur Adipisicing Elit, Sed Do Eiusmod Tempor Incididunt</h6> -->
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px">
                <li><a href="#" class="text-grey-4">Home</a></li>
                <li><a href="#" class="text-grey-4">Artikel</a></li>
                <li class="active"><?php echo $article->title ?></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- // Page title --->

    <!-- page output -->
    <div class="padding-tb-40px">
        <div class="container">
            <div class="row justify-content-center">

                <!--  content -->
                <div class="col-12 sticky-content">

                    <!-- post -->
                    <div class="blog-entry" >
                        <div class="padding-t-30px">
                            <h1 class="d-block text-capitalize text-single-header text-dark font-weight-700 margin-bottom-10px" href="<?php echo article_url($article) ?>"><?php echo $article->title ?></h1>

                            <div class="meta">
                                <span class="margin-right-5px text-extra-small">Oleh <?php echo $article->author_name ?> (<?php echo $article->author_email ?>) <?php echo $article->author_work ?></span>
                                <span class="margin-right-10px text-extra-small">tanggal <?php echo getDateFullNoTime($article->created_at) ?></span>
                                <!-- <span class="text-extra-small">Categorie :  <a href="#" class="text-main-color">Arts</a></span> -->
                            </div>
                        </div>

                        <?php if($article->featured_image){ ?>
                        <!-- <div class="img-in padding-top-30px"><img src="<?php echo base_url() . $article->featured_image ?>" alt=""></div> -->
                        <!-- <div class="img-in padding-top-30px"><img src="<?php echo base_url() . $article->featured_image ?>" alt=""></div> -->
                        <div class="img-in padding-top-30px">
                        <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper " data-alias="varia-slider" data-source="gallery" style="background:transparent;padding:0px;">
                                <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
                                <div id="rev_slider_11_1" class="rev_slider " style="display:none;" data-version="5.4.1">
                                    <ul>

                                        
                                            <!-- SLIDE  -->
                                            <li data-index="rs-<?php echo $article->id ?>" data-transition="fade" data-slotamount="default" >
                                                <!-- MAIN IMAGE -->
                                                <img src="<?php echo base_url() . $article->featured_image ?>" alt="" data-no-retina>
                                                <!-- LAYERS -->

                                            </li>
                                        
                                    </ul>
                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php } ?>

                        <div class="padding-tb-30px">
                            <div class="post-entry">
                                <div class="d-block text-up-small">
                                   <?php echo $article->content ?>
                                </div>

                                <?php if($article->pdf){ 

                                    $download_link = "articles/download/" . $article->id . "/".$article->title."?id=". random_string('numeric', 16);

                                    $fdownload_link = base_url() . "articles/fdownload/" . $article->id . "/".$article->title."?id=". random_string('numeric', 16);

                                    ?>
                                    <!-- <div class="text-center mt-4">
                                        <a href="<?php echo $fdownload_link ?>" class="btn btn-success btn-sm">Unduh</a> -->

                                    <a href="#" class="btn btn-dark btn-sm" onclick="openFullscreen()">Lihat Layar Penuh</a>
                                    </div>

                                    <iframe id="readHere" class="mt-3" src="<?php echo base_url() ?>dist/pdfjs/web/viewer.html?file=../../../<?php echo $download_link ?>" frameborder="0"></iframe>

                                    
                                    <script type="text/javascript">
                                        var elem = document.getElementById("readHere");

                                        /* Function to open fullscreen mode */
                                        function openFullscreen() {
                                          if (elem.requestFullscreen) {
                                          elem.requestFullscreen();
                                          } else if (elem.mozRequestFullScreen) { /* Firefox */
                                          elem.mozRequestFullScreen();
                                          } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
                                          elem.webkitRequestFullscreen();
                                          } else if (elem.msRequestFullscreen) { /* IE/Edge */
                                          elem.msRequestFullscreen();
                                          }
                                        }
                                    </script>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- // post -->

                    <!-- // Author  -->


                    


                </div>
                <!-- //  content -->

            </div>
        </div>
    </div>
    <!-- //  page output -->

<script type="text/javascript">
    var tpj = jQuery;

    var revapi11;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_11_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_11_1");
        } else {
            revapi11 = tpj("#rev_slider_11_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "//localhost/revslider-standalone/revslider/public/assets/js/",
                    // sliderLayout: "fullwidth",
                    sliderLayout: "auto",
                    dottedOverlay: "none",
                    // autoHeight: 'on',
                    // fullScreenOffsetContainer: "#slider_varia",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "hephaistos",
                            hide_onleave: false,
                            direction: "vertical",
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0,
                            space: 5,
                            tmp: ''
                        }
                    },
                    // responsiveLevels: [1240, 1024, 778, 480],
                    // visibilityLevels: [1240, 1024, 778, 480],
                    // gridwidth: [1170, 900, 778, 480],
                    // gridheight: [555, 555, 500, 400],
                    lazyType: "none",
                    shadow: 0,
                    spinner: "spinner4",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    // autoHeight: "off",
                    // fullScreenAutoWidth: "off",
                    // fullScreenAlignForce: "off",
                    // fullScreenOffsetContainer: "",
                    // fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
        }
    }); /*ready*/

</script>

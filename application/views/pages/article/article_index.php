
    <!-- Page title --->
    <section class="background-grey-1 padding-tb-25px text-grey-4">
        <div class="container">
            <h6 class="font-weight-300 text-capitalize float-md-left font-2 padding-tb-10px">Kumpulan <?php echo $category->name ?></h6>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="#" class="text-grey-4">Home</a></li>
                <li class="active">Kumpulan <?php echo $category->name ?></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- // Page title --->


    <!-- page output -->
    <div class="padding-tb-40px background-light-grey">
        <div class="container">
            <div class="row justify-content-center">

                <!--  content -->
                <div class="col-lg-8 col-md-8 sticky-content">

                    <?php foreach ($articles as $article) { ?>
                    <!-- post -->
                    <div class="blog-entry background-white border-1 border-grey-1 margin-bottom-15px">
                        <div class="row no-gutters">
                            <?php if($article->featured_image) { ?>
                                <div class="img-in col-lg-5">
                                    <a href="#"><img src="<?php echo base_url() . $article->featured_image ?>" alt=""></a></div>
                                <div class="col-lg-7">
                            <?php }else{ ?>
                                <div class="col-lg-12">
                            <?php } ?>
                                <div class="padding-25px">
                                    <a class="d-block  text-capitalize text-up-small text-dark font-weight-700 margin-bottom-8px" href="<?php echo article_url($article) ?>"><?php echo $article->title ?></a>
                                    <div class="d-block text-up-small text-grey-2 margin-bottom-10px"><?php echo limit_text(strip_tags($article->content), 15) ?></div>
                                    <div class="meta">
                                        <span class="margin-right-5px text-extra-small">Oleh <?php echo $article->author_name ?></span>
                                        <span class="text-extra-small">Date : <?php echo getDateFullNoTime($article->created_at) ?></span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- // post -->
                    <?php } ?>

                    <?php echo $this->pagination->create_links(); ?>

                    <!-- pagination -->
                    <!-- <ul class="pagination pagination-md ">
                        <li class="page-item disabled"><a class="page-link rounded-0" href="#" tabindex="-1">Previous</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link rounded-0" href="#">Next</a></li>
                    </ul> -->
                    <!-- // pagination -->

                </div>
                <!-- //  content -->


            </div>
        </div>
    </div>
    <!-- //  page output -->


    <div id="instgram-feed">
        <ul class="instagram-feed">
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
            <li><a href="#"><img src="http://placehold.it/400x400" alt=""></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- // Instgram Feed -->


    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row padding-tb-100px">
                <div class="col-lg-6">
                    <div class="about">
                        <div class="logo margin-bottom-20px"><a href="#"><img src="images/logo.png" alt=""></a></div>
                        <p class="text-grey-2">
                            Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida at, accumsan bibendum tincidunt imperdiet. Maecenas quis magna faucibus, varius ante sit amet, varius augue. Praesent aliquam, a imperdiet lacus libero ac tellus. Nunc fringilla ullamcorper quam at lacinia.
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <ul class="footer-menu row margin-0px padding-0px list-unstyled">
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Home</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Featured</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Feedback</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Ask a Question</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Team</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Maintenance</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Get a Quote</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Contact Us</a></li>
                        <li class="col-lg-6 col-md-6 padding-tb-5px"><a href="#" class="text-main-color">Alerts messages</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <ul class="images-feed row no-gutters margin-0px padding-0px list-unstyled">
                        <li class="col-lg-4 col-md-4 padding-tb-5px"><a href="#" class="padding-lr-5px d-block"><img src="http://placehold.it/400x400" alt=""></a></li>
                        <li class="col-lg-4 col-md-4 padding-tb-5px"><a href="#" class="padding-lr-5px d-block"><img src="http://placehold.it/400x400" alt=""></a></li>
                        <li class="col-lg-4 col-md-4 padding-tb-5px"><a href="#" class="padding-lr-5px d-block"><img src="http://placehold.it/400x400" alt=""></a></li>
                        <li class="col-lg-4 col-md-4 padding-tb-5px"><a href="#" class="padding-lr-5px d-block"><img src="http://placehold.it/400x400" alt=""></a></li>
                        <li class="col-lg-4 col-md-4 padding-tb-5px"><a href="#" class="padding-lr-5px d-block"><img src="http://placehold.it/400x400" alt=""></a></li>
                        <li class="col-lg-4 col-md-4 padding-tb-5px"><a href="#" class="padding-lr-5px d-block"><img src="http://placehold.it/400x400" alt=""></a></li>
                    </ul>
                </div>
            </div>

            <div class="row padding-tb-30px border-top-1 border-grey-1">
                <div class="col-lg-4">
                    <p class="text-sm-center text-lg-left"><span class="text-third-color">Travelz</span> | @2017 All copy rights reserved</p>
                </div>
                <div class="col-lg-4 sm-mb-20px">
                    <div class="text-center"><img src="images/cards.png" alt=""></div>
                </div>
                <div class="col-lg-4">
                    <ul class="social_link list-inline text-sm-center text-lg-right">
                        <li class="list-inline-item"><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="youtube" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="google" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                    </ul>
                    <!-- // Social -->
                </div>
            </div>

        </div>

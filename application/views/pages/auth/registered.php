
    <section class="background-light-grey padding-tb-50px text-center text-lg-left">
        <div class="container">
            <h5 class="font-weight-400 text-uppercase text-center">Sukses</h5>
            <div class="row justify-content-md-center">



                <div class="col-lg-4">
                    <div class="padding-30px background-white border-1 border-grey-1">
                        Akun Anda berhasil didaftarkan, silahkan login untuk melanjutkan
                    </div>
                </div>
<!-- 
                <div class="col-lg-4 col-md-6">
                    <div class="margin-bottom-20px margin-top-20px">
                        <i class="fa fa-key margin-right-10px" aria-hidden="true"></i> <strong class="text-uppercase">Sign up Now</strong>
                        <span class="d-block text-grey-2">By signing up you accept our <a href="#" class="text-grey-2">Terms of service</a></span>
                    </div>

                    <p class="text-grey-2">Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida faucibus, varius ante sit amet. </p>
                </div> -->

            </div>
            <!-- // row -->
        </div>
        <!-- // container -->
    </section>


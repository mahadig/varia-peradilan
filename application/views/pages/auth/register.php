


    <section class="background-light-grey padding-tb-50px text-center text-lg-left">
        <div class="container">
            <h5 class="font-weight-400 text-uppercase text-center mb-4">Pendaftaran</h5>
            <div class="row justify-content-md-center">



                <div class="col-lg-6">
                    <div class="padding-30px background-white border-1 border-grey-1">
                    
                        <?php if(isset($message)){ ?>
                            <div class="alert alert-danger" role="alert">
                              <?php echo $message;?>
                            </div>
                        <?php } ?>

                        <form method="POST" action="">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-form-label"><strong><?php echo lang('create_user_fullname_label', 'fullname');?></strong></label>
                                
                                <?php echo form_input($fullname);?>
                                <?php echo form_error('fullname', '<div class="text-danger">', '</div>'); ?>
                            </div>
                           

                            <div class="form-group">
                              <label for="identity" class="col-form-label"><strong><?php echo lang('create_user_identity_label', 'identity');?></strong></label>
                              <?php
                                echo form_input($identity);
                                echo form_error('identity', '<div class="text-danger">', '</div>');
                              ?>
                            </div>    

                            <div class="form-group">
                              <label for="email" class="col-form-label"><strong><?php echo lang('create_user_email_label', 'email');?></strong></label>
                              <?php
                                echo form_input($email);
                                echo form_error('email', '<div class="text-danger">', '</div>');
                              ?>
                            </div>

                            <div class="form-group">
                              <label for="phone" class="col-form-label"><strong><?php echo lang('create_user_phone_label', 'phone');?></strong></label>
                              <?php
                                echo form_input($phone);
                                echo form_error('phone', '<div class="text-danger">', '</div>');
                              ?>
                            </div>

                            <div class="form-group">
                              <label for="job_name" class="col-form-label"><strong><?php echo lang('create_user_jobname_label', 'job_name');?></strong></label>
                              <?php
                                echo form_input($job_name);
                                echo form_error('job_name', '<div class="text-danger">', '</div>');
                              ?>
                            </div>

                            <div class="form-group">
                              <label for="company" class="col-form-label"><strong><?php echo lang('create_user_company_label', 'company');?></strong></label>
                              <?php
                                echo form_input($company);
                                echo form_error('company', '<div class="text-danger">', '</div>');
                              ?>
                            </div>  

                            <div class="form-group">
                              <label for="password" class="col-form-label"><strong><?php echo lang('create_user_password_label', 'password');?></strong></label>
                              <?php
                                echo form_input($password);
                                echo form_error('password', '<div class="text-danger">', '</div>');
                              ?>
                            </div>   

                            <div class="form-group">
                              <label for="password" class="col-form-label"><strong><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></strong></label>
                              <?php
                                echo form_input($password_confirm);
                                echo form_error('password_confirm', '<div class="text-danger">', '</div>');
                              ?>
                            </div>

                            <div class="mb-3 text-center">
                              Dengan menekan tombol "Daftar" di bawah ini, saya setuju dengan  syarat dan ketentuan
                            </div>
                            

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block rounded-0 background-main-color">Daftar</button>
                            </div>
                        </form>
                    </div>
                </div>
<!-- 
                <div class="col-lg-4 col-md-6">
                    <div class="margin-bottom-20px margin-top-20px">
                        <i class="fa fa-key margin-right-10px" aria-hidden="true"></i> <strong class="text-uppercase">Sign up Now</strong>
                        <span class="d-block text-grey-2">By signing up you accept our <a href="#" class="text-grey-2">Terms of service</a></span>
                    </div>

                    <p class="text-grey-2">Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida faucibus, varius ante sit amet. </p>
                </div> -->

            </div>
            <!-- // row -->
        </div>
        <!-- // container -->
    </section>


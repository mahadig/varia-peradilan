
    <section class="background-light-grey padding-tb-50px text-center text-lg-left">
        <div class="container">
            <h5 class="font-weight-400 text-uppercase text-center">Lupa Kata Sandi</h5>
            <div class="row justify-content-md-center">



                <div class="col-lg-6">
                    <div class="padding-30px background-white border-1 border-grey-1">

                    	<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                    
                        <?php if(isset($message)){ ?>
                            <div class="alert alert-danger" role="alert">
                              <?php echo $message;?>
                            </div>
                        <?php } ?>

                        <form method="POST" action="">
                            <div class="form-group">
                                <label for="identity" class="col-form-label"><strong>
									Username atau Email
                                </strong></label>
                                
                                <?php echo form_input($identity);?>
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block rounded-0 background-main-color">Kirimkan Email</button>
                            </div>

                        </form>
                    </div>
                </div>
<!-- 
                <div class="col-lg-4 col-md-6">
                    <div class="margin-bottom-20px margin-top-20px">
                        <i class="fa fa-key margin-right-10px" aria-hidden="true"></i> <strong class="text-uppercase">Sign up Now</strong>
                        <span class="d-block text-grey-2">By signing up you accept our <a href="#" class="text-grey-2">Terms of service</a></span>
                    </div>

                    <p class="text-grey-2">Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida faucibus, varius ante sit amet. </p>
                </div> -->

            </div>
            <!-- // row -->
        </div>
        <!-- // container -->
    </section>



    <section class="background-light-grey padding-tb-50px text-center text-lg-left">
        <div class="container">
            <h5 class="font-weight-400 text-uppercase text-center">Login</h5>
            <div class="row justify-content-md-center">



                <div class="col-lg-4">
                    <div class="padding-30px background-white border-1 border-grey-1">
                    
                        <?php if(isset($message)){ ?>
                            <div class="alert alert-danger" role="alert">
                              <?php echo $message;?>
                            </div>
                        <?php } ?>

                        <form method="POST" action="">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-form-label"><strong><?php echo lang('login_identity_label', 'identity');?></strong></label>
                                
                                <?php echo form_input($identity);?>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-form-label"><strong><?php echo lang('login_password_label', 'password');?></strong></label>
                                <?php echo form_input($password);?>
                            </div>
                            <div class="form-group ">
                                <div class="form-check">
                                    <label class="form-check-label">
                                      <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> 
                                      <?php echo lang('login_remember_label', 'remember');?>
                                    </label>

    
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block rounded-0 background-main-color">Login</button>
                            </div>

                            <a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
                        </form>
                    </div>
                </div>
<!-- 
                <div class="col-lg-4 col-md-6">
                    <div class="margin-bottom-20px margin-top-20px">
                        <i class="fa fa-key margin-right-10px" aria-hidden="true"></i> <strong class="text-uppercase">Sign up Now</strong>
                        <span class="d-block text-grey-2">By signing up you accept our <a href="#" class="text-grey-2">Terms of service</a></span>
                    </div>

                    <p class="text-grey-2">Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida faucibus, varius ante sit amet. </p>
                </div> -->

            </div>
            <!-- // row -->
        </div>
        <!-- // container -->
    </section>


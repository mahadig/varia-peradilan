
    <!-- Page title --->
    <section class="background-grey-1 padding-tb-25px text-grey-4">
        <div class="container">
            <h5 class="font-weight-300 text-capitalize float-md-left font-2 padding-tb-10px">Varia Peradilan: <?php echo $item->title ?></h5>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="<?php echo base_url() ?>" class="text-grey-4">Beranda</a></li>
                <li class="active">Varia Peradilan: <?php echo $item->title ?></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- // Page title --->

    <div class="background-light-grey">


        <!--  shop output -->
        <div class="container padding-tb-40px">
            <div class="mb-3">
                <a href="#" class="btn btn-primary btn-sm" onclick="openFullscreen()">Lihat Layar Penuh</a>
                <a href="#" class="btn btn-success btn-sm">Unduh</a>
            </div>
            <iframe id="readHere" src="<?php echo base_url() ?>dist/pdfjs/web/viewer.html?file=../../../<?php echo $download_link ?>" frameborder="0"></iframe>

        </div>
        
        <script type="text/javascript">
            var elem = document.getElementById("readHere");

            /* Function to open fullscreen mode */
            function openFullscreen() {
              if (elem.requestFullscreen) {
              elem.requestFullscreen();
              } else if (elem.mozRequestFullScreen) { /* Firefox */
              elem.mozRequestFullScreen();
              } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
              elem.webkitRequestFullscreen();
              } else if (elem.msRequestFullscreen) { /* IE/Edge */
              elem.msRequestFullscreen();
              }
            }
        </script>
        <!-- //  shop output -->

    </div>


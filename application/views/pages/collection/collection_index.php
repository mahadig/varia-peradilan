
    <!-- Page title --->
    <section class="background-grey-1 padding-tb-25px text-grey-4">
        <div class="container">
            <h5 class="font-weight-300 text-capitalize float-md-left font-2 padding-tb-10px">Edisi Lainnya</h5>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="<?php echo base_url() ?>" class="text-grey-4">Beranda</a></li>
                <li class="active">Edisi Lainnya</li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- // Page title --->

    <div class="background-light-grey">


        <!--  shop output -->
        <div class="container padding-tb-40px container-collection">

            <div class="row margin-bottom-30px">
                <p class="col-md-9">Menampilkan <?php echo $from ?>-<?php echo $to ?> dari <?php echo $total ?></p>
                <div class="col-md-3 text-right">
                    <select class="custom-select" id="inlineFormCustomSelect">
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                    </select>

                    <button class="btn btn-primary">Filter</button>
                </div>
            </div>


            <!-- -->
            <div class="row">

                <?php 
                foreach ($collection as $row) { 
                
                ?>
                
                <!-- product -->
                <div class="col-lg-2 col-md-6 margin-bottom-40px">
                    <div class="with-hover text-center">
                        <div class="img-thum position-relative hidden">
                            <img src="<?php echo base_url() . $row->cover ?>" alt="">
                        </div>
                        <h3 class="text-large text-center d-block margin-top-10px">
                            <a href="<?php echo base_url() ?>collection/read/<?php echo $row->access_key ?>?id=<?php echo random_string('numeric', 16); ?>" class="text-grey-4"><?php echo $row->title ?></a>
                        </h3>

                        <div class="btn-group my-auto" role="group" aria-label="Basic example">
                                <a href="<?php echo base_url() ?>collection/read/<?php echo $row->access_key ?>?id=<?php echo random_string('numeric', 16); ?>" class="btn btn-warning btn-sm mt-2 btn-block">Baca</a>
                                <a href="<?php echo base_url() ?>collection/fdownload/<?php echo $row->access_key ?>?id=<?php echo random_string('numeric', 16); ?>" class="btn btn-dark btn-sm mt-2 btn-block text-white">Unduh</a>
                            </div>
                        
                    </div>
                </div>
                <!--// product -->

                <?php } ?>

            </div>
            <!--// row -->
<?php echo $this->pagination->create_links(); ?>
            <!-- pagination -->
            <!-- <ul class="pagination pagination-md pagination-style-2 color">
                <li><a class="page-link" href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690" tabindex="-1"><i class="fa fa-chevron-left"></i></a></li>
                <li><a class="page-link" href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690">1</a></li>
                <li><a class="page-link" href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690">2</a></li>
                <li><a class="page-link" href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690">3</a></li>
                <li><a class="page-link" href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690">4</a></li>
                <li><a class="page-link" href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690">5</a></li>
                <li><a class="page-link " href="<?php echo base_url() ?>collection/read/356A192B7913B04C54574D18C28D46E6395428AB?id=9441182075237690"><i class="fa fa-chevron-right"></i></a></li>
            </ul> -->
            <!-- // pagination -->


        </div>
        <!-- //  shop output -->

    </div>


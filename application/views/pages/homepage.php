

<style type="text/css">
    #rev_slider_11_1_wrapper .tp-loader.spinner4 {
        background-color: #FFFFFF !important;
    }

</style>
<style type="text/css">
    .hephaistos .tp-bullet {
        width: 12px;
        height: 12px;
        position: absolute;
        background: rgba(153, 153, 153, 0);
        border: 3px solid rgba(255, 255, 255, 0.9);
        border-radius: 50%;
        cursor: pointer;
        box-sizing: content-box;
        box-shadow: 0px 0px 2px 1px rgba(130, 130, 130, 0.3)
    }

    .hephaistos .tp-bullet:hover,
    .hephaistos .tp-bullet.selected {
        background: rgba(255, 255, 255, 0);
        border-color: rgba(220, 36, 38, 1)
    }

</style>

<section class="background-light-grey slider-section">
    <div class="container p-0">
        <div class="row no-gutters">
            <div class="col-md-4 d-flex justify-content-center">
                <div class="current_edition">
                    <div class="text-center current_edition_wrapper">
                        <div class="post-img padding-top-10px">
                            <a href="<?php echo base_url() ?>collection/read/<?php echo $currentEdition->access_key ?>?id=<?php echo random_string('numeric', 16); ?>"><img src="<?php echo base_url() . $currentEdition->cover ?>" alt="" ></a>
                            <!-- height="336" -->
                        </div>
                        <div class="padding-tb-10px text-center">
                            <a href="<?php echo base_url() ?>collection/read/<?php echo $currentEdition->access_key ?>?id=<?php echo random_string('numeric', 16); ?>" class="d-block text-uppercase"><strong><?php echo $currentEdition->title ?></strong></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8" id="slider_varia">
                <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper " data-alias="varia-slider" data-source="gallery" style="background:transparent;padding:0px;">
                    <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
                    <div id="rev_slider_11_1" class="rev_slider " style="display:none;" data-version="5.4.1">
                        <ul>

                            <?php foreach ($sliders as $item) { ?>
                                <!-- SLIDE  -->
                                <li data-index="rs-<?php echo $item->id ?>" data-transition="fade" data-slotamount="default" >
                                    <!-- MAIN IMAGE -->
                                    <img src="<?php echo base_url() . $item->image ?>" alt="" data-no-retina>
                                    <!-- LAYERS -->

                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END REVOLUTION SLIDER -->
<script type="text/javascript">
    var tpj = jQuery;

    var revapi11;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_11_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_11_1");
        } else {
            revapi11 = tpj("#rev_slider_11_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "//localhost/revslider-standalone/revslider/public/assets/js/",
                    // sliderLayout: "fullwidth",
                    sliderLayout: "auto",
                    dottedOverlay: "none",
                    // autoHeight: 'on',
                    // fullScreenOffsetContainer: "#slider_varia",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "hephaistos",
                            hide_onleave: false,
                            direction: "vertical",
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0,
                            space: 5,
                            tmp: ''
                        }
                    },
                    // responsiveLevels: [1240, 1024, 778, 480],
                    // visibilityLevels: [1240, 1024, 778, 480],
                    // gridwidth: [1170, 900, 778, 480],
                    // gridheight: [555, 555, 500, 400],
                    lazyType: "none",
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    // autoHeight: "off",
                    // fullScreenAutoWidth: "off",
                    // fullScreenAlignForce: "off",
                    // fullScreenOffsetContainer: "",
                    // fullScreenOffset: "",
                    // disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
        }
    }); /*ready*/

</script>

<section class="padding-tb-50px">
    <div class="container">

        <div class="section-title mb-4 section-title-center">
            <h1 class="title">Mengapa Varia Peradilan?</h1>
            <span class="section-title-des"></span>
        </div>


        <div class="row feature-row">
            <div class="col-md-3 col-sm-12 text-center">
                <i class="fa fa-atom"></i>
                <h5>Influencing</h5>
                <span>Memberikan pengaruh yang positif dan konstruktif terhadap pembangunan hukum di Indonesia.</span>
            </div>

            <div class="col-md-3 col-sm-12 text-center mt-4 mt-md-0">
                <i class="fa fa-book-reader"></i>
                <h5>Educating</h5>
                <span>Sarana pembelajaran hukum bagi semua kalangan.</span>
            </div>

            <div class="col-md-3 col-sm-12 text-center mt-4 mt-md-0">
                <i class="fa fa-history"></i>
                <h5>Timeless</h5>
                <span>Tidak akan usang dan lekang oleh waktu serta dapat terus sesuai dengan konteks perkembangan hukum dari masa ke masa.</span>
            </div>

            <div class="col-md-3 col-sm-12 text-center mt-4 mt-md-0">
                <i class="fa fa-lightbulb"></i>
                <h5>Innovative</h5>
                <span>Menyajikan karya-karya yang dapat dijadikan sebagai solusi dan jawaban atas masalah-masalah hukum yang ada di Indonesia</span>
            </div>
        </div>
    </div>
</section>

<?php if (isset($featured_video) && $featured_video){ ?>
<!-- <section class="video-section padding-tb-50px">
    <div class="container">


        <div class="row feature-row justify-content-center">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <video
            crossorigin
            autoplay
            controls data-plyr-config="{ 'control': ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'captions', 'settings', 'pip', 'airplay', 'fullscreen'] }"
            playsinline
            data-poster="<?php echo base_url() . $featured_video->cover ?>"            
            id="player"
          >
            <source
              src="<?php echo base_url() . $featured_video->video ?>"
              autoplay
            />

            <a href="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" download>Download</a>
          </video>
            </div>
        </div>
    </div>
</section> -->
<?php } ?>

<!-- <section class="padding-tb-80px background-light-grey">
    <div class="container">


        <div class="row justify-content-center">

            <div class="col-md-12">
                <?php foreach ($laporan_utama as $row) { ?>
                    <div class="laporan-utama-cart news-cart background-white  sm-mb-30px mb-4">
                        <div class="float-lg-left margin-right-30px sm-mr-0px text-md-center">
                            <img src="<?php echo base_url() . $row->featured_image ?>" alt="">
                        </div>
                        <div class="pv-5 ">
                            <strong class="text-uppercase news-label"><?php echo $row->category_name ?></strong>
                            <h3 class="mb-0"><a href="<?php echo article_url($row) ?>" class="d-block text-dark text-capitalize text-medium mb-0  font-weight-700"><?php echo $row->title ?></a></h3>
                            <span class="margin-right-10px text-extra-small">Oleh <?php echo $row->author_name ?></span>
                            <span class="text-extra-small">Tanggal <?php echo getDateFullNoTime($row->created_at) ?></span>
                            <p class="margin-top-8px text-grey-2"><?php echo limit_text(strip_tags($row->content), 45) ?></p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php } ?>
                
            </div>

        </div>
    </div>
</section> -->

<section class="padding-tb-50px">
    <div class="container">


        <div class="row news-rubic justify-content-center">

            <!-- <div class="col-md-4">
                <strong class="text-uppercase news-label">ANOTASI PUTUSAN</strong>

                <div class="news-cart background-white  sm-mb-30px">
                    <div class="sm-mr-0px text-md-center">
                        <img src="<?php echo base_url() ?>dist/images/sample1.jpg" alt="">
                    </div>
                    <div class="pt-2">
                        <h3 class="mb-0"><a href="<?php echo base_url() ?>article/read/1/lorem-ipsum-dolor-sit-amet-consectetur-adipisicing-elit-sed-do-eiusmod-tempor-incididunt" class="d-block text-dark text-capitalize text-medium font-weight-700">Long Established Fact That A Reader Will Be Distracted By The Readable Content </a></h3>
                        <span class="margin-right-10px text-extra-small">By : <a href="#" class="text-main-color">Rabie Elkheir</a></span>
                        <span class="text-extra-small">Date :  <a href="#" class="text-main-color">July 15, 2016</a></span>
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div> -->

            

            <div class="col-md-7">
                <strong class="text-uppercase news-label">ARTIKEL</strong>
                <?php foreach ($artikel as $item) { ?>
                    <div class="news-cart background-white sm-mb-30px">
                        <?php if($item->featured_image){ ?>
                            <div class="sm-mr-0px text-md-center">
                                <img src="<?php echo base_url() . $item->featured_image ?>" alt="">
                            </div>
                        <?php } ?>
                        <div class="pt-2">

                            <h3 class="mb-0"><a href="<?php echo article_url($item) ?>" class="d-block text-dark text-capitalize text-medium font-weight-700"><?php echo $item->title ?></a></h3>
                            <span class="margin-right-5px text-extra-small">Oleh <?php echo $item->author_name ?></span>
                            <span class="text-extra-small">- <?php echo getDateFullNoTime($item->created_at) ?></span>

                            <p class="margin-top-1px text-grey-2 text-small"><?php echo limit_text(strip_tags($item->content), 18) ?></p>
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php } ?>
            </div>

            <?php if (isset($featured_video) && $featured_video){ ?>
            
                        <div class="col-lg-5 col-md-4 col-sm-12">
                            <video
                        crossorigin
                        autoplay
                        controls data-plyr-config="{ 'control': ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'captions', 'settings', 'pip', 'airplay', 'fullscreen'] }"
                        playsinline
                        data-poster="<?php echo base_url() . $featured_video->cover ?>"            
                        id="player"
                      >
                        <!-- Video files -->
                        <source
                          src="<?php echo base_url() . $featured_video->video ?>"
                          autoplay
                        />


                        <!-- Fallback for browsers that don't support the <video> element -->
                        <a href="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" download>Download</a>
                      </video>
                        </div>
                    
            <?php } ?>


        </div>


    </div>


</div>
</section>


<section class="padding-tb-50px background-light-grey">
    <div class="container">

        <div class="section-title mb-4 section-title-center">
            <h1 class="title">Edisi Lainnya</h1>
            <span class="section-title-des"></span>
        </div>

        <div class="row">
            <?php foreach ($highlight_collection as $hItem) { ?>
                <div class="col-lg-2 col-md-3 mb-md-5 sm-mb-30px">
                    <div class="background-light-grey">
                        <div class="post-img">
                            <a href="<?php echo read_link($hItem) ?>"><img src="<?php echo base_url() . $hItem->cover ?>" alt=""></a>
                        </div>
                        <div class="padding-10px text-center">
                            <a href="#" class="d-block text-dark text-uppercasefont-weight-700 "><?php echo $hItem->title ?></a>
                            <div class="btn-group" role="group" aria-label="Basic example">
                            	<a href="<?php echo read_link($hItem) ?>" class="btn btn-warning btn-sm mt-2 btn-block">Baca</a>
                            	<a href="<?php echo download_link($hItem) ?>" class="btn btn-dark btn-sm mt-2 btn-block text-white">Unduh</a>
                            </div>


                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
</section>






    <!-- Page title --->
    <!-- <section class="background-grey-1 padding-tb-35px text-grey-4">
        <div class="container">
            <h4 class="font-weight-400 text-uppercase float-left font-2">Pricing Tables</h4>
            <ol class="breadcrumb z-index-2 position-relative no-background padding-tb-10px padding-lr-0px  margin-0px float-md-right">
                <li><a href="#" class="text-grey-4">Home</a></li>
                <li><a href="#" class="text-grey-4">pages</a></li>
                <li class="active">Pricing Tables</li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </section> -->
    <!-- // Page title --->

    <section class="background-light-grey padding-tb-50px text-center text-lg-left">
        <div class="container">

            <div class="container z-index-2 position-relative text-center mb-5">
                <h5 class="font-weight-700 text-uppercase">Berlangganan Varia Peradilan</h5>
                <div class="row justify-content-md-center">
                    <div class="col-lg-7 text-grey-2">Mauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida at, accumsan bibendum tincidunt imperdiet. Maecenas quis magna faucibus, varius ante sit amet</div>
                </div>
            </div>

            <div class="row no-gutters">
                <!-- Pricing -->
                <div class="col-lg-3 col-md-3 col-sm-6 sm-mb-40px border-1 border-grey-2">
                    <div class="pricing hover-effect">
                        <div class="pricing-head text-center">
                            <h4 class="padding-15px background-grey-4 margin-0px text-white">Begginer<span class="d-block text-small">Officia deserunt mollitia </span></h4>
                            <h4 class="background-grey-1 margin-0px padding-15px "><i class="text-up-small">Rp </i>150.000
                                <span class="d-block text-small">Per Tahun </span>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled background-white">
                            <li>
                                At vero eos
                            </li>
                            <li>
                                No Support
                            </li>
                            <li>
                                Fusce condimentum
                            </li>
                            <li>
                                Ut non libero
                            </li>
                            <li>
                                Consecte adiping elit
                            </li>
                        </ul>
                        <div class="pricing-footer">
                            <p class="padding-15px margin-0px background-light-grey text-center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
                            </p>
                            <a href="#" class="btn btn-block background-grey-3 text-white rounded-0">
                                Sign Up
                                </a>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-6 sm-mb-40px border-1 border-grey-2">
                    <div class="pricing hover-effect">
                        <div class="pricing-head text-center">
                            <h4 class="padding-15px background-grey-3 margin-0px text-white">Pro<span class="d-block text-small">Officia deserunt mollitia </span></h4>
                            <h4 class="background-grey-1 margin-0px padding-15px "><i class="text-up-small">Rp </i>200.000
                                <span class="d-block text-small">Per Tahun </span>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled background-white">
                            <li>
                                At vero eos
                            </li>
                            <li>
                                No Support
                            </li>
                            <li>
                                Fusce condimentum
                            </li>
                            <li>
                                Ut non libero
                            </li>
                            <li>
                                Consecte adiping elit
                            </li>
                        </ul>
                        <div class="pricing-footer">
                            <p class="padding-15px margin-0px background-light-grey text-center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
                            </p>
                            <a href="#" class="btn btn-block background-grey-4 text-white rounded-0">
                                Sign Up
                                </a>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-6 sm-mb-40px border-1 border-grey-2">
                    <div class="pricing hover-effect">
                        <div class="pricing-head text-center">
                            <h4 class="padding-15px  background-grey-4 margin-0px text-white">Expert<span class="d-block text-small">Officia deserunt mollitia </span></h4>
                            <h4 class="background-grey-1 margin-0px padding-15px "><i class="text-up-small">Rp </i>250.000
                                <span class="d-block text-small">Per Tahun </span>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled background-white">
                            <li>
                                At vero eos
                            </li>
                            <li>
                                No Support
                            </li>
                            <li>
                                Fusce condimentum
                            </li>
                            <li>
                                Ut non libero
                            </li>
                            <li>
                                Consecte adiping elit
                            </li>
                        </ul>
                        <div class="pricing-footer">
                            <p class="padding-15px margin-0px background-light-grey text-center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
                            </p>
                            <a href="#" class="btn btn-block background-grey-3 text-white rounded-0 background-second-color">
                                Sign Up
                                </a>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-6 sm-mb-40px border-1 border-grey-2">
                    <div class="pricing hover-effect">
                        <div class="pricing-head text-center">
                            <h4 class="padding-15px background-grey-3 margin-0px text-white">Hi-Tech
                                <span class="d-block text-small">Officia deserunt mollitia </span></h4>
                            <h4 class="background-grey-1 margin-0px padding-15px "><i class="text-up-small">Rp </i>300.000
                                <span class="d-block text-small">Per Tahun </span>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled background-white">
                            <li>
                                At vero eos
                            </li>
                            <li>
                                No Support
                            </li>
                            <li>
                                Fusce condimentum
                            </li>
                            <li>
                                Ut non libero
                            </li>
                            <li>
                                Consecte adiping elit
                            </li>
                        </ul>
                        <div class="pricing-footer">
                            <p class="padding-15px margin-0px background-light-grey text-center">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
                            </p>
                            <a href="#" class="btn btn-block background-grey-4 text-white rounded-0">
                                Sign Up
                                </a>
                        </div>
                    </div>
                </div>
                <!--//End Pricing -->

            </div>
            <!-- // row -->
        </div>
        <!-- // container -->
    </section>



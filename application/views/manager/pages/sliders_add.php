<?php
$page_title = "Tambah Sliders";

if(isset($id) && $id) {
    $page_title = "Ubah Sliders";
}
?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-8 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $page_title; ?></h4>
                                <h6 class="card-subtitle">Lengkapi kolom-kolom yang tersedia di bawah.</h6>
                            </div>
                            <hr class="m-t-0">
                            <?php if(isset($message)){ ?>
                                <div class="alert alert-danger" role="alert">
                                  <?php echo $message;?>
                                </div>
                            <?php } ?>
                            
                            <form class="form-horizontal r-separator" method="post" enctype="multipart/form-data">
                                <div class="card-body">

                                    <div class="form-group row align-items-center m-b-0">
                                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Judul</label>
                                        <div class="col-9 border-left p-b-10 p-t-10">
                                            <input type="text" class="form-control" value="<?php eissetor($title) ?>" name="title">
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center m-b-0">
                                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Gambar Slider</label>
                                        <div class="col-9 border-left p-b-10 p-t-10">
                                            <div class="input-group">
                                                
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="image" id="inputGroupFile01">
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group m-b-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
           

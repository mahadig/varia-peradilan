
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Artikel</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Artikel</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <?php if($this->session->flashdata('success') != null) { ?>
                    <div class="alert alert-success"> <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                <?php } ?>
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <a href="<?php echo base_url() ?>manager/articles/add" class="btn btn-primary btn-sm">Tambah</a>
                        <table class="tablesaw table-sm table-bordered mt-2 table-hover table" data-tablesaw>
                                    <thead class="thead-dark">
                                        <tr>
                                            
                                            
                                            <th scope="col" class="border text-center">Judul</th>
                                            <th scope="col" class="border text-center" width="170">Kategori</th>
                                            
                                            <th scope="col" class="border text-center" width="180">Diupload Oleh</th>
                                            <th scope="col" class="border text-center" width="180">Diupload Pada</th>
                                            <th scope="col" class="border text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="checkall-target">
                                        <?php foreach ($articles as $item) { ?>
                                            <tr>

                                                <td class="title"><a href="javascript:void(0)"><?php echo $item->title ?></a>
                                                </td>
                                                <td class="text-center"><?php echo $item->category_name ?></td>
                                                <td class="text-center"><?php echo $item->created_by ?></td>
                                                <td class="text-center"><?php echo $item->created_at ?></td>
                                                <td width="150" class="text-center"><a href="<?php base_url() ?>articles/edit/<?php echo $item->id ?>" class="btn btn-sm btn-primary">Sunting</a>
                                                    <a href="<?php base_url() ?>articles/delete?id=<?php echo $item->id ?>" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin ingin menghapus data yang dipilih?')">Hapus</a></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>

                                <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
           

<?php
$page_title = "Tambah Koleksi";

if(isset($id) && $id) {
    $page_title = "Ubah Koleksi";
}
?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-8 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $page_title; ?></h4>
                                <h6 class="card-subtitle">Lengkapi kolom-kolom yang tersedia di bawah.</h6>
                            </div>
                            <hr class="m-t-0">
                            <?php if(isset($message)){ ?>
                                <div class="alert alert-danger" role="alert">
                                  <?php echo $message;?>
                                </div>
                            <?php } ?>
                            
                            <form class="form-horizontal r-separator" method="post" enctype="multipart/form-data">
                                <div class="card-body">

                                    <div class="form-group row align-items-center m-b-0">
                                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Judul</label>
                                        <div class="col-9 border-left p-b-10 p-t-10">
                                            <input type="text" class="form-control" value="<?php eissetor($title) ?>" name="title">
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center m-b-0">
                                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Tanggal Terbit</label>
                                        <div class="col-9 border-left p-b-10 p-t-10">
                                            <input type="date" value="<?php eissetor($publish_date) ?>" class="form-control" name="publish_date">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row align-items-center m-b-0">
                                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Gambar Cover</label>
                                        <div class="col-9 border-left p-b-10 p-t-10">
                                            <div class="input-group">
                                                
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="cover" id="inputGroupFile01">
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row align-items-center m-b-0">
                                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">File PDF</label>
                                        <div class="col-9 border-left p-b-10 p-t-10">
                                            <div class="input-group">
                                                
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="pdf" id="inputGroupFile01">
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <textarea id="summernote" name="content"><?php eissetor($content) ?></textarea>
                                    </div>

                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group m-b-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
           
   
<script type="text/javascript">
        $(document).ready(function(){
            $('#summernote').summernote({
                height: "500px",
                toolbar: [
                  ['font', ['bold', 'underline', 'clear']],
                  ['para', ['ul', 'ol', 'paragraph']],
                  ['insert', ['link', 'picture', 'video']],
                  ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo base_url() ?>manager/articles/upload_image",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "POST",
                    success: function(url) {
                        $('#summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo base_url() ?>manager/articles/delete_image",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
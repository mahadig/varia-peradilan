
<!DOCTYPE html>
<html lang="en-US">

<head>
    <title>Travelz HTML5 Multipurpose Travel Template</title>
    <meta name="author" content="Nile-Theme">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="keywords" content="Travel, HTML5, CSS3, Hotel , Multipurpose, Template, Create a Travel website fast">
    <meta name="description" content="HTML5 Multipurpose Template, Create a website fast">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800|Poppins:300i,400,300,700,400i,500|Pacifico:400%2C700|Raleway:400,500,600,700" rel="stylesheet">
    <!-- CSS Files -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/revslider/css/settings.css">
    <!-- Owl Carousel Assets -->
    <link href="<?php echo base_url() ?>assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/owl.theme.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/travlez-jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/responsive.css">


    <script type="text/javascript" src="<?php echo base_url() ?>assets/js//jquery-3.2.1.min.js"></script>


    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/revslider/js/extensions/revolution.extension.video.min.js"></script>


</head>

<body>

   <!-- Header LAYOUT 6  -->
    <header class="header-style-2">
        <div class="background-main-color padding-tb-5px">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-3 col-md-12">
                        <ul class="list-inline text-center margin-0px text-white">
                            <li class="list-inline-item"><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="youtube" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="google" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                        </ul>
                        <!-- // Social -->
                    </div>
                    <div class="col-lg-6  d-none d-lg-block">
                        <div class="contact-info text-white">
                            <span class="margin-right-10px">Call us : 0022290411185</span>
                            <span>Email: info@localhost.com</span>
                        </div>
                    </div>
                    <div class="col-lg-3  d-none d-lg-block">
                        <ul class="user-area list-inline float-right margin-0px text-white">
                            <li class="list-inline-item  padding-right-10px"><a href="page-login-2.html"><i class="fa fa-lock padding-right-5px"></i>login</a></li>
                            <li class="list-inline-item"><a href="page-login-1.html"><i class="fa fa-user-plus padding-right-5px"></i>register</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-output">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 logo-in text-center">
                        <a id="logo" href="01-home.html" class="d-inline-block margin-tb-20px"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""></a>
                        <a class="mobile-toggle" href="#"><i class="fa fa-navicon"></i></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 inherit-menu header-in border-top-1 border-grey">

                       

                        <div class="search-link pull-right model-link">
                            <a id="search-header" class="model-link" href="#"><i class="fa fa-search"></i></a>
                        </div>

                        <!-- Menu Main -->
                        <ul id="menu-main" class="nav-menu float-left link-padding-tb-15px">
                            <li><a href="<?php echo base_url() ?>">Beranda</a></li>
                            <li><a href="<?php echo base_url() ?>">Varia Peradilan</a></li>
                            <li><a href="<?php echo base_url() ?>">Tentang</a></li>
                            <li><a href="<?php echo base_url() ?>">Hubungi Kami</a></li>
                        </ul>

                        <!-- end Menu Main -->

                    </div>
                </div>
                <div id="search_bar">
                    <a href="#" class="close-search"><i class="fa fa-close"></i></a>
                    <input type="text" class="input-search full-width" placeholder="Masukan kata kunci...">
                </div>
            </div>
        </div>
    </header>
    <!-- // Header  -->



    <style type="text/css">
        #rev_slider_11_1_wrapper .tp-loader.spinner4 {
            background-color: #FFFFFF !important;
        }

    </style>
    <style type="text/css">
        .hephaistos .tp-bullet {
            width: 12px;
            height: 12px;
            position: absolute;
            background: rgba(153, 153, 153, 0);
            border: 3px solid rgba(255, 255, 255, 0.9);
            border-radius: 50%;
            cursor: pointer;
            box-sizing: content-box;
            box-shadow: 0px 0px 2px 1px rgba(130, 130, 130, 0.3)
        }

        .hephaistos .tp-bullet:hover,
        .hephaistos .tp-bullet.selected {
            background: rgba(255, 255, 255, 0);
            border-color: rgba(220, 36, 38, 1)
        }

    </style>

    <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="travel-8" data-source="gallery" style="background:transparent;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_11_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
            <ul>
               
                <!-- SLIDE  -->
                <li data-index="rs-37" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="680" data-thumb="http://placehold.it/100x50" data-delay="9580" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo base_url() ?>assets/images/img1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
    <script type="text/javascript">
        var tpj = jQuery;

        var revapi11;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_11_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_11_1");
            } else {
                revapi11 = tpj("#rev_slider_11_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "//localhost/revslider-standalone/revslider/public/assets/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "hephaistos",
                            hide_onleave: false,
                            direction: "vertical",
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0,
                            space: 5,
                            tmp: ''
                        }
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1170, 900, 778, 480],
                    gridheight: [555, 555, 500, 400],
                    lazyType: "none",
                    shadow: 0,
                    spinner: "spinner4",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

    </script>



    <section class="padding-tb-100px background-light-grey">
        <div class="container">

            <div class="row">
                <?php foreach ($highlight_collection as $hItem) { ?>
                    <div class="col-lg-3 col-sm-6 sm-mb-30px">
                        <div class="background-white border border-grey">
                            <div class="post-img">
                                <a href="#"><img src="<?php echo base_url() ?>assets/images/varia1.jpg" alt=""></a>
                            </div>
                            <div class="padding-10px text-center background-main-color">
                                <a href="#" class="d-block text-white text-uppercase text-medium font-weight-700 "><?php echo $hItem-> ?></a>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="<?php echo base_url() ?>collection/read/<?php echo $hItem->access_key ?>?id=<?php echo random_string('numeric', 16); ?>" class="btn btn-warning btn-sm mt-2 btn-block">Baca</a>
                                    <a class="btn btn-dark btn-sm mt-2 btn-block text-white">Unduh</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>

        </div>
    </section>


    <!-- About -->
    <section class="padding-tb-100px">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <strong class="text-uppercase">Claritas est etiam</strong>
                    <h4>About Us</h4>
                    <p class="text-main-color">Sensectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
                    <p>Sensectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit prae
                    </p>
                    <a href="#" class="btn btn-primary text-uppercase">Read More</a>

                </div>
                <div class="col-md-6">
                    <img src="http://placehold.it/1360x770" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- // About -->




    <section class="padding-tb-100px background-light-grey">
        <div class="container">

            <div class="section-title section-title-center">
                <h1 class="title"><span>Last</span> Blog</h1>
                <span class="section-title-des">Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elitdunt</span>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="news-cart background-white border border-grey sm-mb-30px">
                        <div class="float-lg-left margin-right-30px sm-mr-0px text-md-center">
                            <img src="http://placehold.it/250x250" alt="">
                        </div>
                        <div class="padding-20px">
                            <h3><a href="#" class="d-block text-dark text-capitalize text-medium margin-bottom-15px font-weight-700">Long Established Fact That A Reader Will Be Distracted By The Readable Content </a></h3>
                            <span class="margin-right-20px text-extra-small">By : <a href="#" class="text-main-color">Rabie Elkheir</a></span>
                            <span class="text-extra-small">Date :  <a href="#" class="text-main-color">July 15, 2016</a></span>
                            <p class="margin-top-8px text-grey-2">up a land of wild nature, mystical and unexplored. With only 350,000 visitors per year </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="news-cart background-white border border-grey sm-mb-30px">
                        <div class="float-lg-left margin-right-30px sm-mr-0px text-md-center">
                            <img src="http://placehold.it/250x250" alt="">
                        </div>
                        <div class="padding-20px">
                            <h3><a href="#" class="d-block text-dark text-capitalize text-medium margin-bottom-15px font-weight-700">Long Mauris fermentum porta sem, non hendrerit enim bibendum consectetur </a></h3>
                            <span class="margin-right-20px text-extra-small">By : <a href="#" class="text-main-color">Rabie Elkheir</a></span>
                            <span class="text-extra-small">Date :  <a href="#" class="text-main-color">July 15, 2016</a></span>
                            <p class="margin-top-8px text-grey-2">up a land of wild nature, mystical and unexplored. With only 350,000 visitors per year </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
        </div>
    </section>




    <footer>
        <div class="padding-tb-80px background-dark text-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="logo margin-bottom-20px"><img src="<?php echo base_url() ?>assets/images/ocean-tours-logo.png" alt=""></div>
                        <p class="text-light-grey">
                            Sensectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit prae
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="padding-tb-30px text-center background-black">
            <div class="text-light-grey text-uppercase">Varia Peradilan | @2020 All copy rights reserved</div>
        </div>
    </footer>

    <!-- // Footer -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js//sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js//custom.js"></script>
    <script src="<?php echo base_url() ?>assets/js//owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js//jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js//popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js//bootstrap.min.js"></script>


</body>

</html>

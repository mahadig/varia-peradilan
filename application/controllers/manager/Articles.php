<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/manager/BaseController.php';

class Articles extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('articles_m');

		 if (!$this->ion_auth->is_admin())
	    {
	      exit('Forbidden');
	    }
	}

	function upload_image(){
		$this->load->library('upload');

	    if(isset($_FILES["image"]["name"])){
	    	$tahun = date('Y');
			$bulan = date('m');

			if (!is_dir('resources/images/'.$tahun.'/'.$bulan)) {
				mkdir('./resources/images/'.$tahun.'/'.$bulan, 0755, TRUE);
			}

	        $config['upload_path']      = './resources/images/'.$tahun.'/'.$bulan;
	        $config['allowed_types'] 	= 'jpg|jpeg|png|gif';
	        $config['file_name']		= "image_".time().rand();
	        
	        $this->upload->initialize($config);

	        if(!$this->upload->do_upload('image')){
	            $this->upload->display_errors();
	            return FALSE;
	        }else{
	            $data = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']= './resources/images/' .$tahun.'/'.$bulan . '/' .$data['file_name'];
	            $config['create_thumb']= FALSE;
	            $config['maintain_ratio']= TRUE;
	            $config['quality']= '60%';
	            $config['width']= 800;
	            $config['height']= 800;
	            $config['new_image']= './resources/images/' .$tahun.'/'.$bulan . '/' .$data['file_name'];

	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            echo base_url().'resources/images/' .$tahun.'/'.$bulan . '/' .$data['file_name'];
	        }
	    }
	}

	function delete_image(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully ' . $file_name;
        }
    }

	public function index()
	{
		$jumlah_data = $this->articles_m->frontend_all_count();

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url().'articles/index/';
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= 12;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		
		$from = $this->uri->segment(3);

		$this->pagination->initialize($config);		

		$data['total'] = $jumlah_data;
		$data['from'] = intval($from) + 1;

		$data['to']= $data['from'] + $config['per_page'] - 1;

		$data['articles'] = $this->articles_m->frontend_all_paginate($config['per_page'],$from);

		$this->render('articles_index', $data);
	}

	public function add()
	{
		if($_POST){
			$this->save();
		}else{
			// $this->load->database();
			$this->showForm();
		}
	}

	public function edit($id = false)
	{
		if(!$id){
			redirect(base_url() . 'manager/articles');
		}

		if($_POST){
			$this->save($id);
		}else{
			// $this->load->database();
			$data = $this->articles_m->admin_find_array($id);

			if(!$data) redirect(base_url() . 'manager/articles');

			$this->showForm($id, $data);
		}
	}

	public function delete(){
		$id = intval($this->input->get('id'));
		$delete = 0;

		$item = $this->articles_m->admin_find_array($id);

		if($id > 0 && $item){
			$delete = $this->articles_m->admin_delete($id);
		}

		if($delete != 0){
			unlink($item['image']);

			$this->session->set_flashdata('success', 'Koleksi yang dipilih berhasil dihapus.');
		}

		redirect(base_url() . 'manager/articles');
	}

	private function showForm($id = false, $data = []){
		$data['categories'] = $this->articles_m->categories();

		$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
		$data['id'] = $id;

		$this->render('articles_add', $data);
	}

	private function save($id = false){

		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Judul', 'required');
		$this->form_validation->set_rules('content', 'Konten', 'required');
		$this->form_validation->set_rules('category_id', 'Kategori', 'required');

		$this->form_validation->set_rules('author_name', 'Nama Penulis', 'required');
		$this->form_validation->set_rules('author_email', 'Kategori', 'required');
		$this->form_validation->set_rules('author_work', 'Pekerjaan Penulis', 'required');
		
		// if(!$id){
		// 	if (empty($_FILES['featured_image']['name'])) {
		// 	    $this->form_validation->set_rules('featured_image', 'Gambar Slider', 'required');
		// 	}
		// }

		if($this->form_validation->run() == FALSE){
			$this->showForm();
		}else{
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$category_id = intval($this->input->post('category_id'));
			$author_name = $this->input->post('author_name');
			$author_email = $this->input->post('author_email');
			$author_work = $this->input->post('author_work');

			$category = $this->articles_m->find_category($category_id);
			
			if(!$category) redirect(base_url() . 'manager/articles');

			$data['title'] 			= $title;
			$data['content'] 		= $content;
			$data['category_id']	= $category->id;
			$data['category_name']	= $category->name;
			$data['author_name'] 	= $author_name;
			$data['author_email'] 	= $author_email;
			$data['author_work'] 	= $author_work;

			$featured_image = '';

			/** articles UPLOAD **/
			$tahun = date('Y');
			$bulan = date('m');

			if (!is_dir('resources/images/'.$tahun.'/'.$bulan)) {
				mkdir('./resources/images/'.$tahun.'/'.$bulan, 0755, TRUE);
			}

			$config['upload_path']          = './resources/images/' .$tahun.'/'.$bulan;
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']			= "slider_".time().rand();
			$config['max_size']             = 10000;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('featured_image'))
			{
				if(!empty($_FILES['featured_image']['name'])){
					$data['message'] = '<b>Gambar articles: </b>' . $this->upload->display_errors();

					$this->render('articles_add', $data);
					return false;
				}
			}
			else
			{
				$up = $this->upload->data();
				$featured_image = 'resources/images/' .$tahun.'/'.$bulan . '/' . $up['file_name'];
			}

			// PDF
			$pdf = '';

			/** PDF */
			if(!empty($_FILES['pdf']['name'])){
				$config['upload_path']          = './resources/files/'.$tahun.'/'.$bulan;
				$config['allowed_types']        = 'pdf';
				$config['file_name']			= "pdf_".time().rand();
				$config['max_size']             = 10000;

				// $this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload('pdf'))
				{
					/** IF EDIT MODE IGNORE THIS **/
					$data['message'] = '<b>File PDF: </b>' . $this->upload->display_errors();

					$this->render('article_add', $data);
					return false;
				}
				else
				{
					$up = $this->upload->data();

					$pdf = 'resources/files/'.$tahun.'/'.$bulan .'/' . $up['file_name'];
				}
			}

			/** SAVE */

			if(!$id){
				$data['featured_image'] = $featured_image;
			}else{
				$item = $this->articles_m->admin_find_array($id);

				if($featured_image != ''){
					unlink($item['featured_image']);

					$data['featured_image'] = $featured_image;
				}
			}

			if($pdf != ''){
				$data['pdf'] = $pdf;
			}

			if($id){
				$item = $this->articles_m->admin_find_array($id);

				if($pdf != '' && $item->pdf){
					unlink($item['path']);
				}

				// EDIT
				$data['modified_at'] = date('Y-m-d H:i:a');
				$data['modified_by'] = $user->username;

				$this->articles_m->admin_update($id, $data);
				$this->session->set_flashdata('success', 'Slider baru berhasil disimpan.');
			}else{
				// $generateOrder = $this->articles_m->new_order_number();

				$user = $this->ion_auth->user()->row();

				// $data['order_item'] = $generateOrder;
				$data['created_at'] = date('Y-m-d H:i:a');
				$data['created_by'] = $user->username;

				$this->articles_m->admin_add($data);

				$this->session->set_flashdata('success', 'Slider baru berhasil ditambahkan.');
			}
			
			redirect(base_url() . 'manager/articles');
		}

	}
}

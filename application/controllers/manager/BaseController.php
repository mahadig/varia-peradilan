<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaseController extends CI_Controller {
    function __construct() {
        parent::__construct(); 

        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language']);

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            // redirect them to the login page
            redirect(base_url() . 'manager/auth/login', 'refresh');
        }       
    }

    function render($view, $data = [], $scripts = []){
        $this->load->view('manager/templates/header');
        $this->load->view('manager/pages/' . $view, $data);
        $this->load->view('manager/templates/footer', $scripts);
    }

    function restrict(){
        $userAccess = intval($this->session->userdata('grup_id'));
        
        if(!$userAccess){
            redirect(base_url());
        }
    }
}
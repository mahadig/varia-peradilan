<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/manager/BaseController.php';

class Collection extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('collection_m');

		 if (!$this->ion_auth->is_admin())
	    {
	      exit('Forbidden');
	    }
	}

	public function index()
	{
		$jumlah_data = $this->collection_m->frontend_all_count();

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url().'collection/index/';
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= 12;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		
		$from = $this->uri->segment(3);

		$this->pagination->initialize($config);		

		$data['total'] = $jumlah_data;
		$data['from'] = intval($from) + 1;

		$data['to']= $data['from'] + $config['per_page'] - 1;

		$data['collection'] = $this->collection_m->frontend_all_paginate($config['per_page'],$from);

		$this->render('collection_index', $data);
	}

	public function add()
	{
		if($_POST){
			$this->save();
		}else{
			// $this->load->database();
			$this->showForm();
		}
	}

	public function edit($id = false)
	{
		if(!$id){
			redirect(base_url() . 'manager/collection');
		}

		if($_POST){
			$this->save($id);
		}else{
			// $this->load->database();
			$data = $this->collection_m->admin_find_array($id);

			if(!$data) redirect(base_url() . 'manager/collection');

			$this->showForm($id, $data);
		}
	}

	public function delete(){
		$id = intval($this->input->get('id'));
		$delete = 0;

		$item = $this->collection_m->admin_find_array($id);

		if($id > 0 && $item){
			$delete = $this->collection_m->admin_delete($id);
		}

		if($delete != 0){
			unlink($item['cover']);
			unlink($item['path']);

			$this->session->set_flashdata('success', 'Koleksi yang dipilih berhasil dihapus.');
		}

		redirect(base_url() . 'manager/collection');
	}

	private function showForm($id = false, $data = []){
		$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
		$data['id'] = $id;

		$this->render('collection_add', $data);
	}

	private function save($id = false){

		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Judul', 'required');
		$this->form_validation->set_rules('publish_date', 'Tanggal Terbit', 'required');
		
		if(!$id){
			if (empty($_FILES['cover']['name'])) {
			    $this->form_validation->set_rules('cover', 'Gambar Cover', 'required');
			}

			if (empty($_FILES['pdf']['name'])) {
			    $this->form_validation->set_rules('pdf', 'File PDF', 'required');
			}
		}

		if($this->form_validation->run() == FALSE){
			$this->showForm();
		}else{
			$title = $this->input->post('title');
			$publish_date = $this->input->post('publish_date');

			$data['title'] 			= $title;
			$data['publish_date'] 	= $publish_date;

			$cover = '';
			$pdf = '';

			/** COVER UPLOAD **/
			$tahun = date('Y');
			$bulan = date('m');

			if (!is_dir('resources/covers/'.$tahun.'/'.$bulan)) {
				mkdir('./resources/covers/'.$tahun.'/'.$bulan, 0755, TRUE);
			}

			if (!is_dir('resources/files/'.$tahun.'/'.$bulan)) {
				mkdir('./resources/files/'.$tahun.'/'.$bulan, 0755, TRUE);
			}

			$config['upload_path']          = './resources/covers/'.$tahun.'/'.$bulan;
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']			= "cover_".time().rand();
			$config['max_size']             = 10000;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('cover'))
			{
				if(!empty($_FILES['cover']['name'])){
					$data['message'] = '<b>Gambar Cover: </b>' . $this->upload->display_errors();

					$this->render('collection_add', $data);
					return false;
				}
			}
			else
			{
				$up = $this->upload->data();
				$cover = 'resources/covers/'.$tahun.'/'.$bulan .'/' . $up['file_name'];
			}

			/** PDF */
			$config['upload_path']          = './resources/files/'.$tahun.'/'.$bulan;
			$config['allowed_types']        = 'pdf';
			$config['file_name']			= "pdf_".time().rand();
			$config['max_size']             = 10000;

			$access_key = random_string('sha1');

			// $this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('pdf'))
			{
				/** IF EDIT MODE IGNORE THIS **/
				if(!empty($_FILES['pdf']['name'])){
					$data['message'] = '<b>File PDF: </b>' . $this->upload->display_errors();

					$this->render('collection_add', $data);
					return false;
				}
			}
			else
			{
				$up = $this->upload->data();

				$pdf = 'resources/files/'.$tahun.'/'.$bulan .'/' . $up['file_name'];
			}

			/** SAVE */

			if(!$id){
				$data['access_key'] 	= sha1(time());
				$data['cover'] = $cover;
				$data['path'] = $pdf;
			}else{
				$item = $this->collection_m->admin_find_array($id);

				if($cover != ''){
					unlink($item['cover']);

					$data['cover'] = $cover;
				}
			
				if($pdf != ''){
					unlink($item['path']);

					$data['path'] = $pdf;
				}
			}

			if($id){
				// EDIT
				$data['modified_at'] = date('Y-m-d H:i:a');
				$data['modified_by'] = $user->username;

				$this->collection_m->admin_update($id, $data);
				$this->session->set_flashdata('success', 'Koleksi baru berhasil disimpan.');
			}else{
				$user = $this->ion_auth->user()->row();

				$data['created_at'] = date('Y-m-d H:i:a');
				$data['created_by'] = $user->username;

				$this->collection_m->admin_add($data);
				$this->session->set_flashdata('success', 'Koleksi baru berhasil ditambahkan.');
			}
			
			redirect(base_url() . 'manager/collection');
		}

	}
}

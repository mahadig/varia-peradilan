<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/manager/BaseController.php';

class Sliders extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('sliders_m');

		 if (!$this->ion_auth->is_admin())
	    {
	      exit('Forbidden');
	    }
	}

	public function index()
	{
		$jumlah_data = $this->sliders_m->frontend_all_count();

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url().'sliders/index/';
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= 12;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		
		$from = $this->uri->segment(3);

		$this->pagination->initialize($config);		

		$data['total'] = $jumlah_data;
		$data['from'] = intval($from) + 1;

		$data['to']= $data['from'] + $config['per_page'] - 1;

		$data['sliders'] = $this->sliders_m->frontend_all_paginate($config['per_page'],$from);

		$this->render('sliders_index', $data);
	}

	public function add()
	{
		if($_POST){
			$this->save();
		}else{
			// $this->load->database();
			$this->showForm();
		}
	}

	public function edit($id = false)
	{
		if(!$id){
			redirect(base_url() . 'manager/sliders');
		}

		if($_POST){
			$this->save($id);
		}else{
			// $this->load->database();
			$data = $this->sliders_m->admin_find_array($id);

			if(!$data) redirect(base_url() . 'manager/sliders');

			$this->showForm($id, $data);
		}
	}

	public function delete(){
		$id = intval($this->input->get('id'));
		$delete = 0;

		$item = $this->sliders_m->admin_find_array($id);

		if($id > 0 && $item){
			$delete = $this->sliders_m->admin_delete($id);
		}

		if($delete != 0){
			unlink($item['image']);

			$this->session->set_flashdata('success', 'Koleksi yang dipilih berhasil dihapus.');
		}

		redirect(base_url() . 'manager/sliders');
	}

	private function showForm($id = false, $data = []){
		$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
		$data['id'] = $id;

		$this->render('sliders_add', $data);
	}

	private function save($id = false){

		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Judul', 'required');
		
		if(!$id){
			if (empty($_FILES['image']['name'])) {
			    $this->form_validation->set_rules('image', 'Gambar Slider', 'required');
			}
		}

		if($this->form_validation->run() == FALSE){
			$this->showForm();
		}else{
			$title = $this->input->post('title');

			$data['title'] 			= $title;

			$slider_image = '';

			/** Sliders UPLOAD **/
			$tahun = date('Y');
			$bulan = date('m');

			if (!is_dir('resources/sliders')) {
				mkdir('./resources/sliders', 0755, TRUE);
			}

			if (!is_dir('resources/sliders/')) {
				mkdir('./resources/sliders', 0755, TRUE);
			}

			$config['upload_path']          = './resources/sliders';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']			= "slider_".time().rand();
			$config['max_size']             = 10000;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('image'))
			{
				if(!empty($_FILES['cover']['name'])){
					$data['message'] = '<b>Gambar Sliders: </b>' . $this->upload->display_errors();

					$this->render('sliders_add', $data);
					return false;
				}
			}
			else
			{
				$up = $this->upload->data();
				$slider_image = 'resources/sliders/' . $up['file_name'];
			}

			/** SAVE */

			if(!$id){
				$data['image'] = $slider_image;
			}else{
				$item = $this->sliders_m->admin_find_array($id);

				if($slider_image != ''){
					unlink($item['image']);

					$data['image'] = $slider_image;
				}
			}

			if($id){
				// EDIT
				$data['modified_at'] = date('Y-m-d H:i:a');
				$data['modified_by'] = $user->username;

				$this->sliders_m->admin_update($id, $data);
				$this->session->set_flashdata('success', 'Slider baru berhasil disimpan.');
			}else{
				$generateOrder = $this->sliders_m->new_order_number();

				$user = $this->ion_auth->user()->row();

				$data['order_item'] = $generateOrder;
				$data['created_at'] = date('Y-m-d H:i:a');
				$data['created_by'] = $user->username;

				$this->sliders_m->admin_add($data);

				$this->sliders_m->force_reorder();

				$this->session->set_flashdata('success', 'Slider baru berhasil ditambahkan.');
			}
			
			redirect(base_url() . 'manager/sliders');
		}

	}
}

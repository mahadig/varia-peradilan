<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/manager/BaseController.php';

class Videos extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('videos_m');

		 if (!$this->ion_auth->is_admin())
	    {
	      exit('Forbidden');
	    }
	}

	public function index()
	{
		$jumlah_data = $this->videos_m->frontend_all_count();

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url().'videos/index/';
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= 12;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		
		$from = $this->uri->segment(3);

		$this->pagination->initialize($config);		

		$data['total'] = $jumlah_data;
		$data['from'] = intval($from) + 1;

		$data['to']= $data['from'] + $config['per_page'] - 1;

		$data['videos'] = $this->videos_m->frontend_all_paginate($config['per_page'],$from);

		$this->render('videos_index', $data);
	}

	public function add()
	{
		if($_POST){
			$this->save();
		}else{
			// $this->load->database();
			$this->showForm();
		}
	}

	public function edit($id = false)
	{
		if(!$id){
			redirect(base_url() . 'manager/videos');
		}

		if($_POST){
			$this->save($id);
		}else{
			// $this->load->database();
			$data = $this->videos_m->admin_find_array($id);

			if(!$data) redirect(base_url() . 'manager/videos');

			$this->showForm($id, $data);
		}
	}

	public function delete(){
		$id = intval($this->input->get('id'));
		$delete = 0;

		$item = $this->videos_m->admin_find_array($id);

		if($id > 0 && $item){
			$delete = $this->videos_m->admin_delete($id);
		}

		if($delete != 0){
			unlink($item['video']);
			unlink($item['cover']);

			$this->session->set_flashdata('success', 'Koleksi yang dipilih berhasil dihapus.');
		}

		redirect(base_url() . 'manager/videos');
	}

	private function showForm($id = false, $data = []){
		$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
		$data['id'] = $id;

		$this->render('videos_add', $data);
	}

	private function save($id = false){

		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Judul', 'required');
		
		if(!$id){
			if (empty($_FILES['video']['name'])) {
			    $this->form_validation->set_rules('video', 'Gambar Slider', 'required');
			}

			if (empty($_FILES['cover']['name'])) {
			    $this->form_validation->set_rules('cover', 'Gambar Cover', 'required');
			}
		}

		if($this->form_validation->run() == FALSE){
			$this->showForm();
		}else{
			$title = $this->input->post('title');

			$data['title'] 			= $title;

			$slider_image = '';

			/** videos UPLOAD **/
			$tahun = date('Y');
			$bulan = date('m');

			if (!is_dir('resources/videos/'.$tahun.'/'.$bulan)) {
				mkdir('./resources/videos/'.$tahun.'/'.$bulan, 0755, TRUE);
			}

			if (!is_dir('resources/videos_cover/'.$tahun.'/'.$bulan)) {
				mkdir('./resources/videos_cover/'.$tahun.'/'.$bulan, 0755, TRUE);
			}

			$config['upload_path']          = './resources/videos';
			$config['allowed_types']        = 'mp4|3gp|flv';
			$config['file_name']			= "video_".time().rand();
			$config['max_size']             = 2000000;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('video'))
			{
				if(!empty($_FILES['video']['name'])){
					$data['message'] = '<b>Video: </b>' . $this->upload->display_errors();

					$this->render('videos_add', $data);
					return false;
				}
			}
			else
			{
				$up = $this->upload->data();
				$video_path = 'resources/videos/' . $up['file_name'];
			}

			/** COVER SAVE */

			$config['upload_path']          = './resources/videos_cover/'.$tahun.'/'.$bulan;
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']			= "vid_cover_".time().rand();
			$config['max_size']             = 10000;

			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('cover'))
			{
				if(!empty($_FILES['cover']['name'])){
					$data['message'] = '<b>Sampul Video: </b>' . $this->upload->display_errors();

					$this->render('videos_add', $data);
					return false;
				}
			}
			else
			{
				$up = $this->upload->data();
				$cover_path = 'resources/videos_cover/'.$tahun.'/'.$bulan .'/' . $up['file_name'];
			}

			/** SAVE */

			if(!$id){
				$data['video'] = $video_path;
				$data['cover'] = $cover_path;
			}else{
				$item = $this->videos_m->admin_find_array($id);

				if($video_path != ''){
					unlink($item['video']);

					$data['video'] = $video_path;
				}

				if($cover_path != ''){
					unlink($item['cover']);

					$data['cover'] = $cover_path;
				}
			}

			$data['featured'] = intval($this->input->post('featured'));

			$this->videos_m->resetFeatured();

			if($id){
				// EDIT
				$data['modified_at'] = date('Y-m-d H:i:a');
				$data['modified_by'] = $user->username;

				$this->videos_m->admin_update($id, $data);
				$this->session->set_flashdata('success', 'Video baru berhasil disimpan.');
			}else{
				$generateOrder = $this->videos_m->new_order_number();

				$user = $this->ion_auth->user()->row();

				$data['order_item'] = $generateOrder;
				$data['created_at'] = date('Y-m-d H:i:a');
				$data['created_by'] = $user->username;

				$this->videos_m->admin_add($data);
				$this->videos_m->force_reorder();
				$this->session->set_flashdata('success', 'Video baru berhasil ditambahkan.');
			}
			
			redirect(base_url() . 'manager/videos');
		}

	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Articles extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('articles_m');
	}

	public function index(){
		redirect(base_url());
	}

	public function category($category_id)
	{
		$jumlah_data = $this->articles_m->frontend_all_count($category_id);
		$category = $this->articles_m->find_category($category_id);

		if(!$category){
			redirect(base_url());
		}

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url().'articles/category/' . $category_id;
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= 15;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		
		$from = $this->uri->segment(4);
	

		$this->pagination->initialize($config);		

		$data['total'] = $jumlah_data;
		$data['from'] = intval($from) + 1;

		$data['to']= $data['from'] + $config['per_page'] - 1;

		$data['category'] = $category;
		$data['articles'] = $this->articles_m->frontend_all_paginate($config['per_page'],$from, $category_id);

		// $this->render('articles_index', $data);
		$this->render('article/article_index', $data);
	}

	public function read($id = false)
	{
		if(!$id) redirect(base_url());

		$data['article'] = $this->articles_m->frontend_find($id);

		$this->render('article/article_single', $data);
	}

	public function download($access_key, $dummy = ""){
		$this->load->helper('download');
		

		$item = $this->articles_m->frontend_find($access_key);

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename='OKE.pdf'");
		@readfile($item->pdf);
	}

	public function fdownload($access_key, $dummy = ""){
		$this->load->helper('download');
		$id = $this->input->get('id');

		if(!$access_key || !$id){
			redirect(base_url());
		}

		$item = $this->articles_m->frontend_find($access_key);

		header("Content-type: application/pdf");
		header("Content-Disposition: attachment; filename=$item->title.pdf");
		@readfile($item->pdf);
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Subscribe extends BaseController {

	public function pricing()
	{
		$this->render('subscribe/pricing');
	}
}

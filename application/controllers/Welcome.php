<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Welcome extends BaseController {

	public function index()
	{
		$this->load->model('articles_m');
		$this->load->model('collection_m');
		$this->load->model('sliders_m');
		$this->load->model('videos_m');

		$data['featured_video'] = $this->videos_m->featured_video();
		$data['artikel'] = $this->articles_m->frontend_artikel();
		$data['laporan_utama'] = $this->articles_m->frontend_laporan_utama();
		$data['sliders'] = $this->sliders_m->admin_fetch_all();
		$data['currentEdition'] = $this->collection_m->frontend_current_edition();
		$data['highlight_collection'] = $this->collection_m->frontend_fetch_hightlight();

		$this->render('homepage', $data);
	}
}

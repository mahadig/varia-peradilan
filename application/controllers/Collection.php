<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Collection extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('collection_m');
	}

	public function index()
	{
		$this->load->database();
		
		$jumlah_data = $this->collection_m->frontend_all_count();

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url().'collection/index/';
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= 12;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		
		$from = $this->uri->segment(3);

		$this->pagination->initialize($config);		

		$data['total'] = $jumlah_data;
		$data['from'] = intval($from) + 1;

		$data['to']= $data['from'] + $config['per_page'] - 1;

		$data['collection'] = $this->collection_m->frontend_all_paginate($config['per_page'],$from);

		$this->render('collection/collection_index', $data);
	}

	

	public function download($access_key, $dummy = ""){
		$this->restrict();
		
		$this->load->helper('download');
		$id = $this->input->get('id');

		// if(!$access_key || !$id){
		// 	redirect(base_url());
		// }

		$item = $this->collection_m->frontend_read_collection_item($access_key);

		header("Content-type: application/pdf");
		header("Content-Disposition: filename=$item->title.pdf");
		@readfile($item->path);
	}

	public function fdownload($access_key, $dummy = ""){
		$this->restrict();

		$this->load->helper('download');
		$id = $this->input->get('id');

		if(!$access_key || !$id){
			redirect(base_url());
		}

		$item = $this->collection_m->frontend_read_collection_item($access_key);

		header("Content-type: application/pdf");
		header("Content-Disposition: attachment; filename=$item->title.pdf");
		@readfile($item->path);
	}

	public function read($access_key = "", $dummy = "")
	{
		$this->restrict();
		
		$id = $this->input->get('id');

		if(!$access_key || !$id){
			redirect(base_url());
		}

		$item = $this->collection_m->frontend_read_collection_item($access_key);

		$data['item'] = $item;
		$data['download_link'] = "collection/download/" . $item->access_key . "/".$item->title."?id=". random_string('numeric', 16);

		$this->render('collection/collection_read', $data);
	}
}

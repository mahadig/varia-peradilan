<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaseController extends CI_Controller {
    function __construct() {
        parent::__construct();    

        $this->load->model('counter_m');

        // $this->counter_m->insert();
    }

    function render($view, $data = [], $scripts = []){
        $this->load->view('templates/header');
        $this->load->view('pages/' . $view, $data);
        $this->load->view('templates/footer', $scripts);
    }

    function restrict(){
        if (!$this->ion_auth->logged_in()) {
            if (!empty($_SERVER['QUERY_STRING'])) {
                $uri = uri_string() . '?' . $_SERVER['QUERY_STRING'];
            } else {
                $uri = uri_string();
            }
            $this->session->set_userdata('redirect', $uri);
            
            redirect(base_url() . 'auth/login');
        }
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders_m extends CI_Model
{
	function new_order_number(){
		$this->db->select('count(id) as jml');

		return $this->db->get('sliders')->row()->jml + 1;
	}

	function force_reorder(){
		$this->db->query("
			update sliders target
			join
			(
			     select id, (@rownumber := @rownumber + 1) as rownum
			     from sliders         
			     cross join (select @rownumber := 0) r
			     order by order_item asc
			) source on target.id = source.id    
			set order_item = rownum
		");
	}

	function admin_fetch_all(){
		$this->db->order_by('order_item','asc');
		return $this->db->get('sliders')->result();
	}

	function admin_add($data){
    	$this->db->insert('sliders', $data);

    	return $this->db->insert_id();
    }

    function admin_update($id, $data){
    	return $this->db->update('sliders', $data, array('id' => $id));
    }

    function admin_delete($id){
    	$this->db->delete('sliders', array('id' => $id));

    	return $this->db->affected_rows();
    }

    function admin_find_array($id){
    	$this->db->where('id', $id);

		return $this->db->get('sliders')->row_array();
    }

	function frontend_current_edition(){
		$this->db->limit(1);
		$this->db->order_by('created_at','desc');

		return $this->db->get('sliders')->row();	
	}

	function frontend_fetch_hightlight(){
		$this->db->limit(6);
		$this->db->order_by('created_at','desc');

		return $this->db->get('sliders')->result();	
	}

	function frontend_read_sliders_item($access_key){
		$this->db->where('access_key', $access_key);

		return $this->db->get('sliders')->row();
	}

	function frontend_all_paginate($number,$offset){
		$this->db->order_by('id','desc');

		return $this->db->get('sliders',$number,$offset)->result();
	}

	function frontend_all_count(){
		$this->db->select('count(id) as jml');

		return $this->db->get('sliders')->row()->jml;	
	}
}
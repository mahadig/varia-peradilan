<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_m extends CI_Model
{

	function categories(){
		return $this->db->get('categories')->result();
	}

	function find_category($id){
		$this->db->where('id', $id);

		return $this->db->get('categories')->row();
	}

	function frontend_find($id){
		$this->db->where('articles.id', $id);

		$this->db->join('users as u','u.username = articles.created_by');
		$this->db->select('articles.*');

		return $this->db->get('articles')->row();
	}

	function frontend_laporan_utama(){
		$this->db->limit(5);
		$this->db->order_by('id','desc');
		$this->db->join('users as u','u.username = articles.created_by');
		$this->db->select('articles.*');

		$this->db->where('category_id', 1);

		return $this->db->get('articles')->result();
	}

	function frontend_anotasi_putusan(){
		$this->db->limit(5);
		$this->db->order_by('id','desc');
		$this->db->join('users as u','u.username = articles.created_by');
		$this->db->select('articles.*');

		$this->db->where('category_id', 2);

		return $this->db->get('articles')->result();
	}

	function frontend_artikel(){
		$this->db->limit(5);
		$this->db->order_by('id','desc');
		$this->db->join('users as u','u.username = articles.created_by');
		$this->db->select('articles.*');

		$this->db->where('category_id', 3);

		return $this->db->get('articles')->result();
	}

	
	function admin_fetch_all(){
		// $this->db->order_by('order_item','asc');
		return $this->db->get('articles')->result();
	}

	function admin_add($data){
    	$this->db->insert('articles', $data);

    	return $this->db->insert_id();
    }

    function admin_update($id, $data){
    	return $this->db->update('articles', $data, array('id' => $id));
    }

    function admin_delete($id){
    	$this->db->delete('articles', array('id' => $id));

    	return $this->db->affected_rows();
    }

    function admin_find_array($id){
    	$this->db->where('id', $id);

		return $this->db->get('articles')->row_array();
    }

	function frontend_current_edition(){
		$this->db->limit(1);
		$this->db->order_by('created_at','desc');

		return $this->db->get('articles')->row();	
	}

	function frontend_fetch_hightlight(){
		$this->db->limit(6);
		$this->db->order_by('created_at','desc');

		return $this->db->get('articles')->result();	
	}

	function frontend_read_articles_item($access_key){
		$this->db->where('access_key', $access_key);

		return $this->db->get('articles')->row();
	}

	function frontend_all_paginate($number,$offset, $category_id = false){
		$this->db->order_by('id','desc');

		if($category_id){
			$this->db->where('category_id', $category_id);
		}

		return $this->db->get('articles',$number,$offset)->result();
	}

	function frontend_all_count($category_id = false){
		$this->db->select('count(id) as jml');

		if($category_id){
			$this->db->where('category_id', $category_id);
		}

		return $this->db->get('articles')->row()->jml;	
	}
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Counter_m extends CI_Model
{
	private $sqlite;

	function __construct(){

		parent::__construct();

		$this->sqlite = $this->load->database('sqlite', TRUE);
	}

	function insert(){
		$ip = $this->input->ip_address();

		$query = "INSERT INTO visitor(ip_address,date) 
			SELECT ?, strftime('%Y-%m-%d','now') 
			WHERE NOT EXISTS(SELECT 1 FROM visitor WHERE ip_address = ? AND date = strftime('%Y-%m-%d','now'));";

		$this->sqlite->query($query, [$ip, $ip]);
	}
}
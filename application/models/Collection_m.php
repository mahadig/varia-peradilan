<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Collection_m extends CI_Model
{
	function admin_fetch_all(){
		$this->db->order_by('id','desc');
		return $this->db->get('collection')->result();
	}

	function admin_add($data){
    	$this->db->insert('collection', $data);

    	return $this->db->insert_id();
    }

    function admin_update($id, $data){
    	return $this->db->update('collection', $data, array('id' => $id));
    }

    function admin_delete($id){
    	$this->db->delete('collection', array('id' => $id));

    	return $this->db->affected_rows();
    }

    function admin_find_array($id){
    	$this->db->where('id', $id);

		return $this->db->get('collection')->row_array();
    }

	function frontend_current_edition(){
		$this->db->limit(1);
		$this->db->order_by('publish_date','desc');

		return $this->db->get('collection')->row();	
	}

	function frontend_fetch_hightlight(){
		$this->db->limit(6);
		$this->db->order_by('publish_date','desc');

		return $this->db->get('collection')->result();	
	}

	function frontend_read_collection_item($access_key){
		$this->db->where('access_key', $access_key);

		return $this->db->get('collection')->row();
	}

	function frontend_all_paginate($number,$offset){
		$this->db->order_by('publish_date','desc');

		return $this->db->get('collection',$number,$offset)->result();
	}

	function frontend_all_count(){
		$this->db->select('count(id) as jml');

		return $this->db->get('collection')->row()->jml;	
	}
}
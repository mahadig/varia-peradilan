/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : varia_peradilan

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 01/09/2020 07:36:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `category_name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `featured_image` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `author_name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `author_work` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `author_email` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pdf` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `modified_by` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES (3, 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas', 1, 'Laporan Utama', '<p><span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin aliquam aliquam justo eget commodo. Maecenas lacinia fermentum iaculis. Curabitur tristique rutrum justo ac laoreet. Cras ac vehicula nisl. Vestibulum sed nibh quam. Etiam non felis nibh. Sed vehicula, mauris nec elementum dictum, lectus urna sollicitudin eros, ac iaculis ipsum justo eget dui. Curabitur in imperdiet elit. Pellentesque eu ante nec enim varius feugiat. Pellentesque risus nisl, varius eget dui sed, blandit gravida metus. Suspendisse semper mauris in elit ultrices, non vulputate risus dictum. Morbi a ex vehicula, consequat orci id, tempus enim. Fusce sit amet nisl ut libero molestie faucibus quis nec elit.</span></p><p><img src=\"http://localhost/emag/resources/images/2020/08/image_159852438617655.JPG\" style=\"width: 722.656px;\"><span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\"><br></span></p><p><span style=\"color: rgb(0, 0, 0);\">Vestibulum sed nibh quam. Etiam non felis nibh. Sed vehicula, mauris nec elementum dictum, lectus urna sollicitudin eros, ac iaculis ipsum justo eget dui. Curabitur in imperdiet elit. Pellentesque eu ante nec enim varius feugiat. Pellentesque risus nisl, varius eget dui sed, blandit gravida metus. Suspendisse semper mauris in elit ultrices, non vulputate risus dictum. Morbi a ex vehicula, consequat orci id, tempus enim. Fusce sit amet nisl ut libero molestie faucibus quis nec elit.</span><br></p>', 'resources/images/2020/08/slider_159852219527929.JPG', 'AHMAD Z. ANAM, S.HI., M.H. ', 'Hakim Pengadilan Agama Pasir Pangaraian', 'az_anam_judge@yahoo.com', NULL, '2020-08-27 16:56:00', 'admin', '2020-08-27 18:46:00', NULL);
INSERT INTO `articles` VALUES (7, 'Prosedur Autentifikasi Alat Bukti Elektronik Pada Pemeriksaan Persidangan', 3, 'Artikel', '<p>Teknologi informasi mengalami perkembangan dan kemajuan yang sangat pesat serta telah mengubah paradigma manusia dalam berkomunikasi, yang ditandai dengan maraknya penggunaan sarana teknologi interconnected network (internet) atau media elektronik lainnya. Melalui media elektronik, masyarakat memasuki dunia maya yang bersifat abstrak, universal, lepas dari keadaan, tempat dan waktu.  Teknologi informasi saat ini menjadi pedang bermata dua, karena selain memberikan kontribusi bagi peningkatan kesejahteraan, kemajuan dan peradaban manusia, sekaligus menjadi sarana efektif dalam melakukan perbuatan melawan hukum<br></p>', '', 'HAPPY TRY SULISTIYONO, S.H., M.H.', 'Hakim Pengadilan Negeri Sumedang', 'hakimhappyts@gmail.com', 'resources/files/2020/08/pdf_15985438055126.pdf', '2020-08-27 22:09:00', 'admin', '2020-08-27 22:56:00', NULL);
INSERT INTO `articles` VALUES (8, 'Membangun Laboratorium Bukti Elektronik ', 3, 'Artikel', '<p>Hukum acapkali berjalan tertatih-tatih di belakang pesatnya perkembangan zaman. Fenomena ini - hemat penulis-masih dalam batas kewajaran. Zaman memang boleh melaju sekencang-kencangnya, sepesat-pesatnya, tanpa harus terlebih dahulu menunggu “restu” dari siapa pun, termasuk dari aturan hukum. Sedangkan hukum, sebagai a tool of social engineering , harus berjalan dengan regulasi yang ekstra hati-hati, demi terwujudnya pranata yang bermanfaat, adil, dan pasti.<br></p>', '', 'AHMAD Z. ANAM, S.HI., M.H. ', 'Hakim Pengadilan Agama Pasir Pangaraian', 'az_anam_judge@yahoo.com', 'resources/files/2020/08/pdf_15985423434617.pdf', '2020-08-27 22:10:00', 'admin', '2020-08-27 22:32:00', NULL);
INSERT INTO `articles` VALUES (9, 'Kekuatan Alat Bukti Elektronik Dalam Persidangan Perkara Jinayat', 3, 'Artikel', '<p>Hukum pembuktian (law of evidence) dalam berperkara merupakan bagian yang sangat kompleks dalam proses litigasi. Keadaan kompleksitasnya semakin rumit, karena pembuktian berkaitan dengan kemampuan merekonstruksi kejadian atau peristiwa masa lalu (past event) sebagai suatu kebenaran. Meskipun kebenaran yang dicari dan diwujudkan dalam proses peradilan perdata, bukan kebenaran yang bersifat absolut (ultimate truth), tetapi bersifat kebenaran relatif atau bahkan cukup bersifat kemungkinan (probable), namun untuk kebenaran yang demikian saja tetap menghadapi kesulitan,  apalagi dalam proses peradilan pidana termasuk dalam peradilan pidana Islam (jinayat) yang harus mencari kebenaran materiil.<br></p>', '', 'NURHADI, S.HI., MH.', 'Hakim Mahkamah Syar’iyah Sabang', 'nurhadi1980@gmail.com', 'resources/files/2020/08/pdf_15985439553324.pdf', '2020-08-27 22:59:00', 'admin', '2020-08-28 01:08:00', NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `slug` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Laporan Utama', 'laporan_utama');
INSERT INTO `categories` VALUES (2, 'Anotasi Putusan', 'anotasi_putusan');
INSERT INTO `categories` VALUES (3, 'Artikel', 'artikel');

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `access_key` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `publish_date` date NULL DEFAULT NULL,
  `cover` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hit` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `modified_by` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collection
-- ----------------------------
INSERT INTO `collection` VALUES (1, '356A192B7913B04C54574D18C28D46E6395428AB', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (2, 'DA4B9237BACCCDF19C0760CAB7AEC4A8359010B0', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (3, '77DE68DAECD823BABBB58EDB1C8E14D7106E83BB', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (4, '1B6453892473A467D07372D45EB05ABC2031647A', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (5, 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (6, 'AC3478D69A3C81FA62E60F5C3696165A4E5E6AC4', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (7, 'c1dfd96eea8cc2b62785275bca38ac261256e278', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (8, 'fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (9, '0ade7c2cf97f75d009975f4d720d1fa6c19f4897', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (10, 'b1d5781111d84f7b3fe45a0852e59758cd7a87e5', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (11, '17ba0791499db908433b80f37c5fbc89b870084b', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (12, '7b52009b64fd0a2a49e6d8a939753077792b0554', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (13, 'bd307a3ec329e10a2cff8fb87480823da114f8f4', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (14, 'fa35e192121eabf3dabf9f5ea6abdbcbc107ac3b', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (15, 'f1abd670358e036c31296e66b3b66c382ac00812', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (16, '1574bddb75c78a6fd2251d61e2993b5146201319', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (17, '0716d9708d321ffb6a00818614779e779925365c', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (18, '9e6a55b6b4563e652a23be9d623ca5055c356940', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (19, 'b3f0c7f6bb763af1be91d9e74eabfeb199dc1f1f', 'No. 304 Maret 2011', '2020-08-27', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', NULL, NULL);
INSERT INTO `collection` VALUES (20, '91032ad7bbcb6cf72875e8e8207dcfba80173f7c', 'No. 304 Maret 2019', '2020-08-26', 'resources/covers/sample.jpg', 'resources/files/sample.pdf', NULL, '2020-08-25 11:14:41', 'admin', '2020-08-27 12:07:00', NULL);
INSERT INTO `collection` VALUES (27, '1b1e4931881aa92efade99a0299a4e6b4fba6795', 'No. 999 Agustus 2020', '2020-08-28', 'resources/covers/2020/08/cover_159851642125709.jpg', 'resources/files/2020/08/pdf_159850523810239.pdf', NULL, '2020-08-27 12:08:00', 'admin', '2020-08-27 15:20:00', NULL);
INSERT INTO `collection` VALUES (28, 'ad08cbadb64d8dbd01b2a94c0e9a65fe4244fb19', 'No. 008 September 2020', '2020-09-01', 'resources/covers/2020/08/cover_159855108417468.jpg', 'resources/files/2020/08/pdf_159855108425546.pdf', NULL, '2020-08-28 00:58:00', 'admin', '2020-08-29 16:33:00', NULL);

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------
INSERT INTO `login_attempts` VALUES (1, '::1', 'root', 1598302245);
INSERT INTO `login_attempts` VALUES (2, '::1', 'root', 1598302283);
INSERT INTO `login_attempts` VALUES (3, '::1', 'root', 1598302410);

-- ----------------------------
-- Table structure for packages
-- ----------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price` int(10) NULL DEFAULT NULL,
  `months` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sliders
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `order_item` int(10) UNSIGNED NULL DEFAULT NULL,
  `link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `modified_by` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sliders
-- ----------------------------
INSERT INTO `sliders` VALUES (2, 'Tes lagi', 'resources/sliders/slider_159851584511031.jpg', 2, NULL, '2020-08-27 15:10:00', 'admin', NULL, NULL);
INSERT INTO `sliders` VALUES (3, 'test slider', 'resources/sliders/slider_159851619917690.jpg', 3, NULL, '2020-08-27 15:16:00', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_email`(`email`) USING BTREE,
  UNIQUE INDEX `uc_activation_selector`(`activation_selector`) USING BTREE,
  UNIQUE INDEX `uc_forgotten_password_selector`(`forgotten_password_selector`) USING BTREE,
  UNIQUE INDEX `uc_remember_selector`(`remember_selector`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'admin', '$2y$12$chDBzemBIotknh8dk5LTme1OsEINma4hwfQ0C.jcH.Au2qlnFgc4e', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1598779928, 1, NULL, 'Admin', 'istrator', 'ADMIN', NULL, '0');
INSERT INTO `users` VALUES (2, '::1', 'mahadi', '$2y$10$diR/RvbhFg7M79f/ZJuoi.Dld0fmTX31Ko3RQpC6yE8K.JT5vbXYK', 'mahadiwpi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1598791212, 1598808391, 1, 'Mahadi Ganda Perwira', NULL, NULL, 'PA Sumber', 'Programmer', '081288820654');
INSERT INTO `users` VALUES (4, '::1', 'mahadia', '$2y$10$Tw84wgKiduaxFU2mDo.Eue4GAfIejkLXACfbovNHqFUBXvwJL6cDO', 'mahadiwpia@gmail.com', NULL, NULL, NULL, NULL, NULL, '77b0f97ec64eabf5837e99771e500f23a8ed49d5', '$2y$10$cRCb2vn4vY8VYyyaFdSqCeAx5T53IPqu5iMYdWxKNXkhWGP.IWEce', 1598805685, 1598805685, 1, 'Mahadi Ganda Perwira', NULL, NULL, 'PA Sumber', 'Programmer', '081288820654');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (2, 1, 2);
INSERT INTO `users_groups` VALUES (3, 2, 2);
INSERT INTO `users_groups` VALUES (4, 4, 2);

SET FOREIGN_KEY_CHECKS = 1;

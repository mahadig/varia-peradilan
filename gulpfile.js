var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');
var del = require('del');

gulp.task('clean-js', function () {
  return del([
    'dist/js/*.js'
  ]);
});

gulp.task('clean-css', function () {
  return del([
    'dist/css/*.css'
  ]);
});

gulp.task('pack-js', gulp.series('clean-js', function () {  
  return gulp.src(
    [
    'assets/js//jquery-3.2.1.min.js',
    'assets/js/pdfobject.min.js',
    'assets/plugins/revslider/js/jquery.themepunch.tools.min.js',
    'assets/plugins/revslider/js/jquery.themepunch.revolution.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.actions.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.carousel.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.kenburn.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.layeranimation.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.migration.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.navigation.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.parallax.min.js',
    'assets/plugins/revslider/js/extensions/revolution.extension.slideanims.min.js',
    'assets/js/custom.js',
    'assets/js/owl.carousel.min.js',
    'assets/js/jquery-ui.js',
    'assets/js/popper.min.js',
    'assets/js/bootstrap.min.js'
    ]
    ).pipe(concat('bundle.js'))
    // .pipe(minify({
    //     ext:{
    //         min:'.js'
    //     },
    //     noSource: true
    // }))
    .pipe(rev())
    .pipe(gulp.dest('dist/js'))
    .pipe(rev.manifest('dist/rev-manifest.json', {
        merge: true,
        base: 'dist'
    }))
    .pipe(gulp.dest('dist'));
}));

gulp.task('pack-css', gulp.series('clean-css', function () {  
  return gulp.src(
    [
      'assets/plugins/revslider/css/settings.css',
      'assets/css/owl.carousel.css',
      'assets/css/owl.theme.css',
      'assets/fonts/font-awesome5/css/all.css',
      'assets/css/bootstrap.min.css',
      'assets/css/travlez-jquery-ui.css',
      'assets/css/style.css',
      'assets/css/responsive.css',
      'assets/css/custom.css'
    ])
    .pipe(concat('style.css'))
    .pipe(cleanCss())
    .pipe(rev())
    .pipe(gulp.dest('dist/css'))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      merge: true,
      base: 'dist'
    }))
    .pipe(gulp.dest('dist'));
}));

gulp.task('watch', function() {
  gulp.watch('assets/js/**/*.js', gulp.series('pack-js'));
  gulp.watch('assets/css/**/*.css', gulp.series('pack-css'));
});

gulp.task('default', gulp.series('watch'));